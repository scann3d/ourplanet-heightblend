﻿Shader "HeightBlend/SurfaceBlendCombo"
{
    Properties
    {

        //Effectmode
        _vfxmode("VFX mode",float) = 0.0

        //Rendermode
        _Mode("blend mode",float) = 0.0

        [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend("__src", int) = 0
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend("__dst", int) = 1
        _ZWrite("__zw", int) = 0

        //Revealing
        _AlphaMod("Alpha modification",Range(-1.0,1.0)) = 0.0
        _GeneralAlpha("General Transparency", Range(0.0,2.0)) = 1.5
        _RevealMask("Revealing mask",2D) = "white" {}

        //DistanceFade
        _FadeRadius("FadeRadius",Range(0.0,10.0)) = 0.0
        _FadePosition3D("Fade Center Position in 3D space",Vector) = (0,0,0,0)
        _FadeScaleFactor("Fade Scale Factor (XY)",Vector) = (1,1,0,0)
        _FadeWidth("FadeWidth",Range(0.1,20.0)) = 0.1

        //DynamicDisplacement
        _SubDisplace("Dynamic displacement Max",float) = 0.5

         
        //blendMask
        _MaskTex("Mask Texture", 2D) = "black" {}
        _MaskOffset("Mask Offset", Range(-1.0, 1.0)) = 0.0
        //blendEdge
        _EdgeColorA("Edge ColorA", Color) = (1, 0, 0, 1)
        _EdgeColorB("Edge ColorB", Color) = (0, 1, 0, 1)
        _EdgeValue("Edge Value", Range(0, 1)) = 0.5
        _EdgeWidth("Edge Width", Range(0, 0.5)) = 0.5
        _EdgeNoise("Edge Noise", 2D) = "white" {}
        _EdgeSpeed("Edge Speed", Vector) = (0.1, -0.08, 0, 0)


        //wipe 
        _WipePosition("Wipe Position", Range(-1.0, 2.0)) = 0.5
        _WipeWidth("Wipe Width", Range(0.0, 1.0)) = 0.1
        [Enum(Horizontal, 0, Vertical, 1)] _WipeDirection("Wipe Direction", int) = 0
        _WipeOffset("Wipe Offset", float) = 0
        _DisplacementPinchDistance("Displacement Pinch Distance", Range(0.0, 1.0)) = 0.0
        _DisplacementPinchOffset("Displacement Pinch Offset", Range(0.0, 1.0)) = 0.0

        //Tesselation
        _Tess ("Tesselation Factor", Range(1, 32)) = 4
        _MinDist("Minimum Distance", Range(0, 20)) = 1
        _MaxDist("Maximum Distance", Range(0, 20)) = 5


        _OceanSpeedA("Ocean Speed A", Vector) = (0.1, -0.13, 0, 0)
        _OceanSpeedB("Ocean Speed B", Vector) = (-0.09, 0.21, 0, 0)

        _AlbedoA ("Albedo A", 2D) = "white" {}
        _MetallicA ("MetallicA", 2D) = "black" {}
        _SmoothnessA("SmoothnessA", Range(0.0, 1.0)) = 0.0
        [Normal] _NormalA ("NormalA", 2D) = "bump" {}
        _OcclusionA ("Occlusion A", 2D) = "white" {}
        _DisplacementA ("Displacement A", 2D) = "grey" {}
        _EmissionA ("Emission A", 2D) = "black" {}
        
        _DisplacementScaleA ("Displacement Scale A", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA ("Displacement Offset A", Range(-1.0, 1.0)) = 0.0
        
        _AlbedoB ("Albedo B", 2D) = "white" {}
        _MetallicB ("Metallic B", 2D) = "black" {}
        _SmoothnessB("SmoothnessA", Range(0.0, 1.0)) = 0.0
        [Normal] _NormalB ("Normal B", 2D) = "bump" {}
        _OcclusionB ("Occlusion B", 2D) = "white" {}
        _DisplacementB ("Displacement B", 2D) = "grey" {}
        _EmissionB ("Emission B", 2D) = "black" {}
        

        _AlbedoC("Albedo C", 2D) = "black" {}
        [Normal] _NormalC("Normal C", 2D) = "bump" {}
        _OcclusionC("Occlusion C", 2D) = "white" {}
        _DisplacementC("Displacement C", 2D) = "black"{}

        _DisplacementScaleB ("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB ("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

		
        

    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200

        
        Blend [_SrcBlend] [_DstBlend]
        
        //ZWrite [_ZWrite]
        ZWrite On

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        //#pragma surface surf Standard noforwardadd vertex:vert tessellate:tessDistance keepalpha 
        #pragma surface surf Standard noforwardadd vertex:vert keepalpha 

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.6
        
        #pragma shader_feature Shader_Is_blend
        #pragma shader_feature Shader_Is_Oceanblend
        #pragma shader_feature SLOTA_IS_EMPTY
        #pragma shader_feature SLOTB_IS_EMPTY
        #pragma shader_feature A_Is_Ocean
        #pragma shader_feature B_Is_Ocean
        #pragma shader_feature _HAILALPHABLEND

        //#include "Tessellation.cginc"

        sampler2D _MainTex;


        struct Input
        {
            float2 uv_MaskTex;
            //float2 uv_AlbedoA;
            //float2 uv_AlbedoB;

            //float3 localPos;
            float3 worldPos;
            
            float3 texcoord : TEXCOORD0;

            fixed4 color : COLOR;
            float2 uv_RevealMask;
            //float2 uv_AlbedoC;
        };

        //wipe stuff
        float _WipePosition;
        float _WipeWidth;
        int _WipeDirection;
        float _WipeOffset;

        //blendMask stuff
        sampler2D _MaskTex;
        float _MaskOffset;

        //tessellation stuff
        int _Tess;
        float _MinDist;
        float _MaxDist;
        
        float _DisplacementPinchDistance;
        float _DisplacementPinchOffset;
        
        //edge stuff
        float4 _EdgeColorA;
        float4 _EdgeColorB;
        float _EdgeValue;
        float _EdgeWidth;
        //sampler2D _EdgeNoise;
        //float4 _EdgeNoise_ST;
        float2 _EdgeSpeed;

        //Reveal mask
        sampler2D _RevealMask;
        half _GeneralAlpha;
        half _AlphaMod;

        //BoundaryFade
        float _FadeRadius;
        float4 _FadePosition3D;
        float4 _FadeScaleFactor;
        float _FadeWidth;

        //DynamicDisplacement
        fixed _SubDisplace;

        //waves stuff
        float2 _OceanSpeedA;
        float2 _OceanSpeedB;
        
        //texture A stuff
        sampler2D _AlbedoA;
        float _SmoothnessA;
        float4 _AlbedoA_ST;
        sampler2D _MetallicA;
        sampler2D _NormalA;
        sampler2D _OcclusionA;
        sampler2D _DisplacementA;
        //sampler2D _EmissionA;
        
        float _DisplacementScaleA;
        float _DisplacementOffsetA;
        
        //texture B stuff
        sampler2D _AlbedoB;
        float _SmoothnessB;
        float4 _AlbedoB_ST;
        sampler2D _MetallicB;
        sampler2D _NormalB;
        sampler2D _OcclusionB;
        sampler2D _DisplacementB;
        //sampler2D _EmissionB;
        
        float _DisplacementScaleB;
        float _DisplacementOffsetB;


        //texture C stuff
        sampler2D _AlbedoC;
        float4 _AlbedoC_ST;
        sampler2D _NormalC;
        sampler2D _OcclusionC;
        sampler2D _DisplacementC;
        

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        //UNITY_INSTANCING_BUFFER_START(Props)
        //    // put more per-instance properties here
        //UNITY_INSTANCING_BUFFER_END(Props)
        


//        float4 tessDistance(appdata_full v0, appdata_full v1, appdata_full v2) {
//#ifdef Shader_Is_blend
//            return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _MinDist, _MaxDist, _Tess);
//#else
//            return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, 0, 0, 1.0);
//#endif
//        }


        //just to make the code easier to read/write
        //blends between two texture samples
        float4 tex2D_blend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float blendFactor)
        {
//#ifdef Shader_Is_blend
            //return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactor);

//#else
            #ifdef B_Is_Ocean
			float2 oceanuv1 = texcoordB + float2(0.01123, -0.0085) * _Time.y;
			float2 oceanuv2 = texcoordB + float2(-0.00961, 0.0155321) * _Time.y;
			float4 ocean1 = tex2D(b, oceanuv1);
			float4 ocean2 = tex2D(b, oceanuv2);
			float4 ocean = min(ocean1, ocean2);

			return lerp(tex2D(a, texcoordA), ocean, blendFactor);

            #elif A_Is_Ocean
			float2 oceanuv1 = texcoordA + float2(0.01123, -0.0085) * _Time.y;
			float2 oceanuv2 = texcoordA + float2(-0.00961, 0.0155321) * _Time.y;
			float4 ocean1 = tex2D(a, oceanuv1);
			float4 ocean2 = tex2D(a, oceanuv2);
			float4 ocean = min(ocean1, ocean2);

			return lerp(ocean, tex2D(b, texcoordB), blendFactor);

            #else
			
			return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactor);
            #endif
//#endif
        }
        
        float4 tex2D_doubleAnimated(sampler2D tex, float2 texcoord, float2 speedA, float2 speedB)
        {
            float2 texcoordA = texcoord + speedA * _Time.y;
            float2 texcoordB = texcoord + speedB * _Time.y;
            float4 colA = tex2D(tex, texcoordA);
            float4 colB = tex2D(tex, texcoordB);
            colB = float4(colB.r, colB.g, colB.b, colB.r);
            float4 colmin = min(colA, colB);
            float4 colmax = max(colA, colB);

            float4 collerp = lerp(colA, colB, clamp(abs(sin(_Time.y * 0.2)), 0.4, 0.6));
            float4 collerp2 = lerp(colmin, collerp, abs(sin(_Time.y * 0.5)));


            return collerp2;
        }

        //blends between A and [BC], with BC being a blend between B and C
        float4 tex2D_tripleBlend(sampler2D a, sampler2D b, float4 colC, float2 texcoordA, float2 texcoordB, float2 speedA, float2 speedB, float blendFactorAB, float blendFactorBC)
        {
            float4 colA = tex2D_doubleAnimated(a, texcoordA, speedA, speedB);
            float4 colB = tex2D_doubleAnimated(b, texcoordB, speedA, speedB);

            return lerp(colA, lerp(colB, colC, blendFactorBC), blendFactorAB);
        }

        float4 tex2D_3LayerBlend_noocean(sampler2D a, sampler2D b, float4 c, float2 texcoordA, float2 texcoordB, float blendFactorAB, float blendFactorBC) {

            return lerp(tex2D(a, texcoordA), lerp(tex2D(b, texcoordB), c, blendFactorBC), blendFactorAB);
        }

        //just to make the code easier to read/write
        //blends between two texture samples (using tex2Dlod i.e. from the vertex shader)
        float4 tex2Dlod_blend(sampler2D a, sampler2D b, sampler2D c, float4 texcoordA, float4 texcoordB, float4 texcoordC ,float blendFactor , float blendfactorBC)
        {

            #ifdef B_Is_Ocean
			float4 oceanuv1 = texcoordB + float4(0.01123, -0.0085,0,0) * _Time.y;
			float4 oceanuv2 = texcoordB + float4(-0.00961, 0.0155321,0,0) * _Time.y;
			float4 ocean1 = tex2Dlod(b, oceanuv1);
			float4 ocean2 = tex2Dlod(b, oceanuv2);
			float4 ocean = min(ocean1, ocean2);

			return lerp(tex2Dlod(a, texcoordA), ocean, blendFactor);

            #elif A_Is_Ocean
			float4 oceanuv1 = texcoordA + float4(0.01123, -0.0085,0,0) * _Time.y;
			float4 oceanuv2 = texcoordA + float4(-0.00961, 0.0155321,0,0) * _Time.y;
			float4 ocean1 = tex2Dlod(a, oceanuv1);
			float4 ocean2 = tex2Dlod(a, oceanuv2);
			float4 ocean = min(ocean1, ocean2);

			return lerp(ocean, tex2Dlod(b, texcoordB), blendFactor);

            #else


            return lerp(tex2Dlod(a, texcoordA), lerp(tex2Dlod(b, texcoordB), tex2Dlod(c, texcoordC), blendfactorBC), blendFactor);

            #endif
        }
        

        float4 tex2Dlod_doubleBlend(sampler2D a, sampler2D b, float4 texcoordA, float4 texcoordB, float2 speedA, float2 speedB, float blendFactor)
        {
            float4 a_i = texcoordA + float4(speedA, 0, 0) * _Time.y;
            float4 a_ii = texcoordA + float4(speedB, 0, 0) * _Time.y;
            float4 b_i = texcoordB + float4(speedA, 0, 0) * _Time.y;
            float4 b_ii = texcoordB + float4(speedB, 0, 0) * _Time.y;
            float4 blend_i = lerp(tex2Dlod(a, a_i), tex2Dlod(b, b_i), blendFactor);
            float4 blend_ii = lerp(tex2Dlod(a, a_ii), tex2Dlod(b, b_ii), blendFactor);
            return lerp(blend_i, blend_ii, 0.5);
            //return lerp(tex2Dlod(a, texcoordA), tex2Dlod(b, texcoordB), blendFactor);
        }

        //float getDisplacementPinch(float distance, float2 texcoord)
        //{
        //    if(_DisplacementPinchDistance <= 0) return 1.0;
        //    
        //    float top = saturate((1.0 - (texcoord.y + _DisplacementPinchOffset)) / _DisplacementPinchDistance);
        //    float bottom = saturate((texcoord.y - _DisplacementPinchOffset) / _DisplacementPinchDistance);
        //    return top * bottom;
        //}

        float getBlendFactorSpherical_3D(float3 pos, float width, float radius, float3 pixelworldpos, float3 scalefactor) {

            float vx = (pixelworldpos.x - pos.x);
            float vy = (pixelworldpos.y - pos.y);
            float vz = (pixelworldpos.z - pos.z);

            float3 v = float3 (vx, vy, vz);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2) + pow(v.z / scalefactor.z, 2)));

            return saturate(sphericalArea / radius + width);
        }


        float getBlendFactorSpherical_local(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);



            float posy = (pos.y);
            float vy = (uv.y - posy);


            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            radius /= width;

            return  saturate((sphericalArea/ radius - width));

        }

        float getBlendFactorSpherical_local_vert(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);



            float posy = (pos.y);
            float vy = (uv.y - posy);


            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            return  saturate(sphericalArea / radius);

        }



        
        float getBlendFactor(float pos, float size, int direction, float offset, float2 uv)
        {
            if(direction <= 0) {
                float relativePos = ((uv.x + offset) % 1.0) - pos;
                relativePos += size;
                return 1.0 - saturate(relativePos / (size * 2.0));
            } else {
                float relativePos = ((uv.y + offset) % 1.0) - pos;
                relativePos += size;
                return 1.0 - saturate(relativePos / (size * 2.0));
            }
        }
        
        void vert (inout appdata_full v)
        {

            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
#ifdef Shader_Is_blend

            float blendFactor = tex2Dlod(_MaskTex, v.texcoord).r;
            blendFactor = saturate(blendFactor + _MaskOffset);
#else

            float blendFactor = getBlendFactor(_WipePosition, _WipeWidth, _WipeDirection, _WipeOffset, v.texcoord.xy);

#endif

            

            //get the displacement factor and transform based on properties
            float4 texcoordA = float4(v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw, 1.0, 1.0);
            float4 texcoordB = float4(v.texcoord.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw, 1.0, 1.0);
            float4 texcoordC = float4(v.texcoord.xy * _AlbedoC_ST.xy + _AlbedoC_ST.zw, 1.0, 1.0);
            float thirdlayer = tex2Dlod(_AlbedoC, texcoordC).a;

#ifdef Shader_Is_Oceanblend
            float displacementAmount = tex2Dlod_doubleBlend(_DisplacementA, _DisplacementB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor);
#else
            float displacementAmount = tex2Dlod_blend(_DisplacementA, _DisplacementB, _DisplacementC, texcoordA, texcoordB, texcoordC, blendFactor, thirdlayer);
#endif
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);
            //displacementAmount *= getDisplacementPinch(_DisplacementPinchDistance, v.texcoord.xy);
            
            //actually offset the vertices - note that y = up
            //v.vertex += float4(v.normal * displacementAmount, 0.0);
            
            float3 worldpos = mul(unity_ObjectToWorld, v.vertex);
            float CycleFactor = getBlendFactorSpherical_local_vert(_FadePosition3D.xz, 0.1, _FadeRadius * 1.5, worldpos.xz, _FadeScaleFactor);
            float dynamicdisplace = lerp(_SubDisplace, 0.0, CycleFactor) * _FadePosition3D.y;
            
            v.vertex += float4(0, displacementAmount + dynamicdisplace, 0, 0);
            v.color.xy = texcoordA.xy;
            v.color.zw = texcoordB.xy;
            //o.localPos = v.vertex.xyz;
            //o.texcoordA = texcoordA.xy;
            //o.texcoordB = texcoordB.xy;
            //o.texcoord = v.texcoord;

        }

        void surf_main (Input IN, inout SurfaceOutputStandard o, float blendFactor)
        {
            float2 texcoordC = float2(IN.uv_MaskTex.xy * _AlbedoC_ST.xy + _AlbedoC_ST.zw);
            float4 netAlbedo = tex2D(_AlbedoC, texcoordC);
            float4 netNormal = tex2D(_NormalC, texcoordC);
            float4 netOcclu = tex2D(_OcclusionC, texcoordC);
            //float4 netmask = tex2D(_MaskC, IN.uv_AlbedoC);

            float2 texcoordA = IN.color.xy;
            float2 texcoordB = IN.color.zw;

            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
            
            #ifdef SLOTA_IS_EMPTY
            if(blendFactor < 0.5) discard;
            #endif

            #ifdef SLOTB_IS_EMPTY
			if (blendFactor > 0.5) discard;
            #endif

            
            fixed4 color = fixed4(0,0,0,1);

             //lerp between all the maps and set them to the appropriate output channels
            //o.Albedo = lerp(tex2D(_AlbedoA, IN.texcoordA), tex2D(_AlbedoB, IN.texcoordB), blendFactor);
#ifdef Shader_Is_Oceanblend

            o.Albedo = tex2D_tripleBlend(_AlbedoA, _AlbedoB, netAlbedo, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a);
            o.Smoothness = lerp(_SmoothnessA, _SmoothnessB, blendFactor);
            o.Normal = UnpackNormal(tex2D_tripleBlend(_NormalA, _NormalB, netNormal, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a));
            o.Occlusion = tex2D_tripleBlend(_OcclusionA, _OcclusionB, float4(1, 1, 1, 1), texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a).r; 

#else 
			//o.Albedo = tex2D_blend(_AlbedoA,_AlbedoB, texcoordA, texcoordB, blendFactor);

            #ifdef B_Is_Ocean
            o.Albedo = tex2D_blend(_AlbedoA, _AlbedoB, texcoordA, texcoordB, blendFactor);
            float4 metalsmooth = tex2D_blend(_MetallicA, _MetallicB, texcoordA, texcoordB, blendFactor);
            o.Normal = UnpackNormal(tex2D_blend(_NormalA, _NormalB, texcoordA, texcoordB, blendFactor));
            o.Occlusion = tex2D_blend(_OcclusionA, _OcclusionB, texcoordA, texcoordB, blendFactor).r;
            o.Smoothness = lerp(metalsmooth.a, 0.8, blendFactor);

            #elif A_Is_Ocean
            o.Albedo = tex2D_blend(_AlbedoA, _AlbedoB, texcoordA, texcoordB, blendFactor);
            float4 metalsmooth = tex2D_blend(_MetallicA, _MetallicB, texcoordA, texcoordB, blendFactor);
            o.Normal = UnpackNormal(tex2D_blend(_NormalA, _NormalB, texcoordA, texcoordB, blendFactor));
            o.Occlusion = tex2D_blend(_OcclusionA, _OcclusionB, texcoordA, texcoordB, blendFactor).r;
            o.Smoothness = lerp(0.8, metalsmooth.a, blendFactor);

            #else
            color = tex2D_3LayerBlend_noocean(_AlbedoA, _AlbedoB, netAlbedo, texcoordA, texcoordB, blendFactor, netAlbedo.a);
            o.Albedo = color.rgb;
            //float4 metalsmooth = tex2D_blend(_MetallicA, _MetallicB, texcoordA, texcoordB, blendFactor);
            float4 metalsmooth = tex2D_3LayerBlend_noocean(_MetallicA, _MetallicB, float4 (0,0,0,0), texcoordA, texcoordB, blendFactor, netAlbedo.a);
            //o.Normal = UnpackNormal(tex2D_blend(_NormalA, _NormalB, texcoordA, texcoordB, blendFactor));
            o.Normal = UnpackNormal(tex2D_3LayerBlend_noocean(_NormalA, _NormalB, netNormal, texcoordA, texcoordB, blendFactor, netAlbedo.a));
            //o.Occlusion = tex2D_blend(_OcclusionA, _OcclusionB, texcoordA, texcoordB, blendFactor).r;
            o.Occlusion = tex2D_3LayerBlend_noocean(_OcclusionA, _OcclusionB, netOcclu, texcoordA, texcoordB, blendFactor, netAlbedo.a).r;
            //o.Emission = tex2D_blend(_EmissionA, _EmissionB, texcoordA, texcoordB, blendFactor).rgb;
            o.Smoothness = metalsmooth.a;

            #endif

#endif
            o.Metallic = 0;
            o.Alpha = color.a;
            
            
            

#ifdef Shader_Is_blend

            if (blendFactor < _EdgeValue) {
                blendFactor = 1.0 - saturate((_EdgeValue - blendFactor) / _EdgeWidth);
            }
            else {
                blendFactor = 1.0 - saturate((blendFactor - _EdgeValue) / _EdgeWidth);
            }

            //float colX = tex2D(_EdgeNoise, IN.uv_MaskTex * _EdgeNoise_ST.xy + float2(_Time.y * _EdgeSpeed.x, 0)).x;
            //float colY = tex2D(_EdgeNoise, IN.uv_MaskTex * _EdgeNoise_ST.xy + float2(0, _Time.y * _EdgeSpeed.y)).x;
            //float4 edgeCol = lerp(_EdgeColorA, _EdgeColorB, (colX + colY) * 0.5);
            float4 edgeCol = _EdgeColorA;

            o.Albedo = lerp(o.Albedo, edgeCol, blendFactor);
            //o.Albedo *= o.Occlusion;

#endif
            o.Albedo *= o.Occlusion;
        }




        void surf(Input IN, inout SurfaceOutputStandard o)
        {

#ifdef Shader_Is_blend
            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
            float blendFactor = tex2D(_MaskTex, IN.uv_MaskTex).r;
            blendFactor = saturate(blendFactor + _MaskOffset);
#else
            
            float blendFactor = getBlendFactor(_WipePosition, _WipeWidth, _WipeDirection, _WipeOffset, IN.uv_MaskTex);
#endif
            
            surf_main(IN, o, blendFactor);


#ifdef _HAILALPHABLEND


            
            float revealmasksample = tex2D(_RevealMask, IN.uv_RevealMask).r;
            half alphamodstep = lerp(0.0, _AlphaMod, revealmasksample);

            float revealAlpha = saturate((revealmasksample + alphamodstep) * _GeneralAlpha);

            //

            //float CycleFactor = getBlendFactorSpherical_3D(_FadePosition3D.xyz, _FadeWidth, _FadeRadius, IN.localPos.xyz, _FadeScaleFactor);
            float CycleFactor = getBlendFactorSpherical_local(_FadePosition3D.xz, _FadeWidth, _FadeRadius, IN.worldPos.xz, _FadeScaleFactor);

            float EdgeFadeAlpha = lerp(1.0, 0.0, CycleFactor);
            
            o.Alpha *= EdgeFadeAlpha * revealAlpha;

#else

            o.Alpha = 1.0;
#endif


        }


        ENDCG
    }


    
    //use the custom editor
    CustomEditor "SurfaceBlendComboGUI"
    FallBack "Diffuse"
}
