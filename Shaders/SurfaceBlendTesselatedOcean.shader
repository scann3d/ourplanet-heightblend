﻿Shader "HeightBlend/SurfaceBlendTessellatedOcean"
{
    Properties
    {
        _MaskTex ("Mask Texture", 2D) = "black" {}
        _MaskOffset ("Mask Offset", Range(-1.0, 1.0)) = 0.0
        
        _Tess ("Tesselation Factor", Range(1, 32)) = 4
        _MinDist ("Minimum Distance", Range(0, 20)) = 1
        _MaxDist ("Maximum Distance", Range(0, 20)) = 5
        
        _EdgeColorA ("Edge ColorA", Color) = (1, 0, 0, 1)
        _EdgeColorB ("Edge ColorB", Color) = (0, 1, 0, 1)
        _EdgeValue ("Edge Value", Range(0, 1)) = 0.5
        _EdgeWidth ("Edge Width", Range(0, 0.5)) = 0.5
        _EdgeNoise ("Edge Noise", 2D) = "white" {}
        _EdgeSpeed ("Edge Speed", Vector) = (0.1, -0.08, 0, 0)
        
        _OceanSpeedA ("Ocean Speed A", Vector) = (0.1, -0.13, 0, 0)
        _OceanSpeedB ("Ocean Speed B", Vector) = (-0.09, 0.21, 0, 0)
        
        _AlbedoA ("Albedo A", 2D) = "white" {}
        _SmoothnessA ("SmoothnessA", Range(0.0, 1.0)) = 0.0
        [Normal] _NormalA ("NormalA", 2D) = "bump" {}
        _OcclusionA ("Occlusion A", 2D) = "white" {}
        _DisplacementA ("Displacement A", 2D) = "grey" {}
        
        _DisplacementScaleA ("Displacement Scale A", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA ("Displacement Offset A", Range(-1.0, 1.0)) = 0.0
        
        _AlbedoB ("Albedo B", 2D) = "white" {}
        _SmoothnessB ("SmoothnessA", Range(0.0, 1.0)) = 0.0
        [Normal] _NormalB ("Normal B", 2D) = "bump" {}
        _OcclusionB ("Occlusion B", 2D) = "white" {}
        _DisplacementB ("Displacement B", 2D) = "grey" {}
        
        _DisplacementScaleB ("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB ("Displacement Offset B", Range(-1.0, 1.0)) = 0.0
        
        _AlbedoC ("Albedo C", 2D) = "black" {}
        [Normal] _NormalC ("Normal C", 2D) = "bump" {}
        _OcclusionC ("Occlusion C", 2D) = "white" {}
        
        _ScaleA ("Scale A", Vector) = (1, 1, 0, 0)
        _ScaleB ("Scale B", Vector) = (1, 1, 0, 0)

        _RevealMask("RevealMask",2D) = "white"{}
        _RevealHelper("RevealHelper",Range(0.0,1.0)) = 1.0

    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert tessellate:tessDistance alpha:blend

        #pragma target 4.6
        #include "Tessellation.cginc"

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MaskTex;
            float2 uv_AlbedoA;
            float2 uv_AlbedoB;
            float2 uv_AlbedoC;
        };

        //mask stuff
        sampler2D _MaskTex;
        float _MaskOffset;
        
        //tessellation stuff
        int _Tess;
        float _MinDist;
        float _MaxDist;
        
        //edge stuff
        float4 _EdgeColorA;
        float4 _EdgeColorB;
        float _EdgeValue;
        float _EdgeWidth;
        sampler2D _EdgeNoise;
        float4 _EdgeNoise_ST;
        float2 _EdgeSpeed;
        
        //waves stuff
        float2 _OceanSpeedA;
        float2 _OceanSpeedB;
        
        //texture A stuff
        sampler2D _AlbedoA;
        float _SmoothnessA;
        sampler2D _NormalA;
        sampler2D _OcclusionA;
        sampler2D _DisplacementA;
        float4 _DisplacementA_ST;
        
        float _DisplacementScaleA;
        float _DisplacementOffsetA;
        
        //texture B stuff
        sampler2D _AlbedoB;
        float _SmoothnessB;
        sampler2D _NormalB;
        sampler2D _OcclusionB;
        sampler2D _DisplacementB;
        float4 _DisplacementB_ST;
        
        float _DisplacementScaleB;
        float _DisplacementOffsetB;
        
        sampler2D _AlbedoC;
        sampler2D _NormalC;
        sampler2D _OcclusionC;
        
        float2 _ScaleA;
        float2 _ScaleB;
        
        //Reveal mask

        sampler2D _RevealMask;
        half _RevealHelper;
        

        float4 tessDistance (appdata_full v0, appdata_full v1, appdata_full v2) {
            return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _MinDist, _MaxDist, _Tess);
        }
        
        //just to make the code easier to read/write
        //blends between two texture samples
        float4 tex2D_blend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float blendFactor)
        {
            return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactor);
        }
        
        float4 tex2D_doubleAnimated(sampler2D tex, float2 texcoord, float2 speedA, float2 speedB)
        {
            float2 texcoordA = texcoord + speedA * _Time.y;
            float2 texcoordB = texcoord + speedB * _Time.y;
            float4 colA = tex2D(tex, texcoordA);
            float4 colB = tex2D(tex, texcoordB);
			colB = float4(colB.r, colB.g, colB.b, colB.r);
			float4 colmin = min(colA, colB);
			float4 colmax = max(colA, colB);

			float4 collerp = lerp(colA, colB, clamp(abs(sin(_Time.y * 0.2)),0.4,0.6));
			float4 collerp2 = lerp(colmin, collerp, abs(sin( _Time.y * 0.5 )) );
			

            return collerp2;
        }
        
        //blends between A and [BC], with BC being a blend between B and C
        float4 tex2D_tripleBlend(sampler2D a, sampler2D b, float4 colC, float2 texcoordA, float2 texcoordB, float2 speedA, float2 speedB, float blendFactorAB, float blendFactorBC)
        {
            float4 colA = tex2D_doubleAnimated(a, texcoordA, speedA, speedB);
            float4 colB = tex2D_doubleAnimated(b, texcoordB, speedA, speedB);
            
            return lerp(colA, lerp(colB, colC, blendFactorBC), blendFactorAB);
        }
        
//        float4 tex2D_doubleBlend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float2 speedA, float2 speedB, float blendFactor)
//        {
//            float2 a_i = texcoordA + speedA * _Time.y;
//            float2 a_ii = -texcoordA + speedB * _Time.y;
//            float2 b_i = texcoordB + speedA * _Time.y;
//            float2 b_ii = -texcoordB + speedB * _Time.y;
//            float4 blend_i = lerp(tex2D(a, a_i), tex2D(b, b_i), blendFactor);
//            float4 blend_ii = lerp(tex2D(a, a_ii), tex2D(b, b_ii), blendFactor);
//            return lerp(blend_i, blend_ii, 1);
//        }
        
        //just to make the code easier to read/write
        //blends between two texture samples (using tex2Dlod i.e. from the vertex shader)
        float4 tex2Dlod_blend(sampler2D a, sampler2D b, float4 texcoordA, float4 texcoordB, float blendFactor)
        {
            return lerp(tex2Dlod(a, texcoordA), tex2Dlod(b, texcoordB), blendFactor);
        }
        
        float4 tex2Dlod_doubleBlend(sampler2D a, sampler2D b, float4 texcoordA, float4 texcoordB, float2 speedA, float2 speedB, float blendFactor)
        {
            float4 a_i = texcoordA + float4(speedA, 0, 0) * _Time.y;
            float4 a_ii = texcoordA + float4(speedB, 0, 0) * _Time.y;
            float4 b_i = texcoordB + float4(speedA, 0, 0) * _Time.y;
            float4 b_ii = texcoordB + float4(speedB, 0, 0) * _Time.y;
            float4 blend_i = lerp(tex2Dlod(a, a_i), tex2Dlod(b, b_i), blendFactor);
            float4 blend_ii = lerp(tex2Dlod(a, a_ii), tex2Dlod(b, b_ii), blendFactor);
            return lerp(blend_i, blend_ii, 0.5);
            //return lerp(tex2Dlod(a, texcoordA), tex2Dlod(b, texcoordB), blendFactor);
        }
        
        void vert (inout appdata_full v)
        {            
            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
            float blendFactor = tex2Dlod(_MaskTex, v.texcoord).r;
            blendFactor = saturate(blendFactor + _MaskOffset);
            
            //get the displacement factor and transform based on properties
            float4 texcoordA = float4(v.texcoord.xy * _ScaleA, 1.0, 1.0);//float4(v.texcoord.xy * _DisplacementA_ST.xy + _DisplacementA_ST.zw, 1.0, 1.0);
            float4 texcoordB = float4(v.texcoord.xy * _ScaleB, 1.0, 1.0);//float4(v.texcoord.xy * _DisplacementB_ST.xy + _DisplacementB_ST.zw, 1.0, 1.0);
            float displacementAmount = tex2Dlod_doubleBlend(_DisplacementA, _DisplacementB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor);
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);
            
            //actually offset the vertices - note that y = up
            v.vertex += float4(0, displacementAmount, 0, 0);//float4(v.normal * displacementAmount, 0.0);
        }
        
        void surf_main(Input IN, inout SurfaceOutputStandard o, float blendFactor)
        {
            float4 netAlbedo = tex2D(_AlbedoC, IN.uv_AlbedoC.xy);
            float4 netNormal = tex2D(_NormalC, IN.uv_AlbedoC.xy);
        
            float2 texcoordA = IN.uv_AlbedoA * _ScaleA;
            float2 texcoordB = IN.uv_AlbedoB * _ScaleB;
        
            o.Albedo = tex2D_tripleBlend(_AlbedoA, _AlbedoB, netAlbedo, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a);
            //float4 metalsmooth = tex2D_tripleBlend(_MetallicA, _MetallicB, float4(0,0,0,1), texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a);
            o.Metallic = 0.0;
            o.Smoothness = lerp(_SmoothnessA, _SmoothnessB, blendFactor);
            o.Normal = UnpackNormal(tex2D_tripleBlend(_NormalA, _NormalB, netNormal, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a));
            o.Occlusion = tex2D_tripleBlend(_OcclusionA, _OcclusionB, float4(1,1,1,1), texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor, netAlbedo.a).r;
            o.Albedo *= o.Occlusion.r;
        
//            //lerp between all the maps and set them to the appropriate output channels
//            o.Albedo = tex2D_doubleBlend(_AlbedoA, _AlbedoB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor);
//            
//            //float4 metalsmooth = tex2D_doubleBlend(_MetallicA, _MetallicB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor);
//            o.Metallic = 0.0;//metalsmooth.r;
//            o.Smoothness = lerp(_SmoothnessA, _SmoothnessB, blendFactor);
//            o.Normal = UnpackNormal(tex2D_doubleBlend(_NormalA, _NormalB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor));
//            o.Occlusion = tex2D_doubleBlend(_OcclusionA, _OcclusionB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor).r;
//            //o.Emission = tex2D_doubleBlend(_EmissionA, _EmissionB, texcoordA, texcoordB, _OceanSpeedA, _OceanSpeedB, blendFactor).rgb;
//            o.Albedo *= o.Occlusion;
        }
        
        void surf_edge(Input IN, inout SurfaceOutputStandard o, float blendFactor)
        {
            if(blendFactor < _EdgeValue) {
                blendFactor = 1.0 - saturate((_EdgeValue - blendFactor) / _EdgeWidth);
            } else {
                blendFactor = 1.0 - saturate((blendFactor - _EdgeValue) / _EdgeWidth);
            }
            
            float colX = tex2D(_EdgeNoise, IN.uv_MaskTex * _EdgeNoise_ST.xy + float2(_Time.y * _EdgeSpeed.x, 0)).x;
            float colY = tex2D(_EdgeNoise, IN.uv_MaskTex * _EdgeNoise_ST.xy + float2(0, _Time.y * _EdgeSpeed.y)).x;
            float4 edgeCol = lerp(_EdgeColorA, _EdgeColorB, (colX + colY) * 0.5);
            
            o.Albedo = lerp(o.Albedo, edgeCol, blendFactor);
            o.Albedo *= o.Occlusion;
            //o.Emission = _EdgeColorB * (colX + colY) * 0.5 * blendFactor;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
            float blendFactor = tex2D(_MaskTex, IN.uv_MaskTex).r;
            blendFactor = saturate(blendFactor + _MaskOffset);
            float revealFactor = tex2D(_RevealMask, IN.uv_MaskTex).r * 1.3;

            surf_main(IN, o, blendFactor);
            surf_edge(IN, o, blendFactor);

            o.Alpha = revealFactor * _RevealHelper;
        }
        ENDCG
    }
    
    //use the custom editor
    //CustomEditor "SurfaceBlendTessellatedGUI"
    FallBack "Diffuse"
}
