﻿Shader "HeightBlend/SurfaceBlend2020_GeneralBlend_transparent"
{
    Properties
    {
        //Revealing
        [Header(Revealing)]
        _AlphaMod("Alpha modification",Range(-1.0,1.0)) = 0.0
        _GeneralAlpha("General Transparency", Range(0.0,2.0)) = 1.5
        _RevealMask("Revealing mask",2D) = "white" {}

        //DistanceFade
        [Space(10)]
        [Header(DistanceFade)]
        [Toggle] _EnableDistanceFade("Enable Distance Fade", int) = 0
        _FadeRadius("FadeRadius",Range(0.0,10.0)) = 0.0
        _FadePosition3D("Fade Center Position in 3D space",Vector) = (0,0,0,0)
        _FadeScaleFactor("Fade Scale Factor (XY)",Vector) = (1,1,0,0)
        _FadeWidth("FadeWidth",Range(0.1,20.0)) = 0.1

        //DynamicDisplacement
        [Space(10)]
        [Header(DynamicDisplacement)]
        _SubDisplace("Dynamic displacement Max",float) = 0.5

        //blendMask
        [Space(10)]
        [Header(Blending)]
        _MaskTex("Mask Texture", 2D) = "black" {}
        _MaskOffset("Mask Offset", Range(-1.0, 1.0)) = 0.0
        //blendEdge
        _EdgeColorA("Edge ColorA", Color) = (1, 0, 0, 1)
        //_EdgeColorB("Edge ColorB", Color) = (0, 1, 0, 1)
        _EdgeValue("Edge Value", Range(0, 1)) = 0.5
        _EdgeWidth("Edge Width", Range(0, 0.5)) = 0.5
        //_EdgeNoise("Edge Noise", 2D) = "white" {}
        _EdgeSpeed("Edge Speed", Vector) = (0.1, -0.08, 0, 0)

        //Specific property
        [Space(10)]
        [Header(OceanSpecific)]
        _OceanSpeedA("Ocean Speed A", Vector) = (0.1, -0.13, 0, 0)
        _OceanSpeedB("Ocean Speed B", Vector) = (-0.09, 0.21, 0, 0)

        //Texture set A
        [Space(10)]
        [Header(Texture Set A)]
        _AlbedoA("Albedo A", 2D) = "white" {}
        _MetallicA("MetallicA", 2D) = "black" {}
        _SmoothnessA("SmoothnessA", Range(0.0, 1.0)) = 0.0
        [Normal] _NormalA("NormalA", 2D) = "bump" {}
        _OcclusionA("Occlusion A", 2D) = "white" {}
        _DisplacementA("Displacement A", 2D) = "grey" {}

        _DisplacementScaleA("Displacement Scale A", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA("Displacement Offset A", Range(-1.0, 1.0)) = 0.0

        //Texture set B
        [Space(10)]
        [Header(Texture Set B)]
        _AlbedoB("Albedo B", 2D) = "white" {}
        _MetallicB("Metallic B", 2D) = "black" {}
        _SmoothnessB("SmoothnessA", Range(0.0, 1.0)) = 0.0
        [Normal] _NormalB("Normal B", 2D) = "bump" {}
        _OcclusionB("Occlusion B", 2D) = "white" {}
        _DisplacementB("Displacement B", 2D) = "grey" {}

        _DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

        //Texture set C
        [Space(10)]
        [Header(Texture Set C)]
        _AlbedoC("Albedo C", 2D) = "black" {}
        [Normal] _NormalC("Normal C", 2D) = "bump" {}
        _OcclusionC("Occlusion C", 2D) = "white" {}
        _DisplacementC("Displacement C", 2D) = "black"{}

        _DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard noforwardadd vertex:vert keepalpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.0

        #pragma shader_feature SLOTA_IS_EMPTY
        #pragma shader_feature SLOTB_IS_EMPTY

        //blendMask stuff
        sampler2D _MaskTex;
        fixed _MaskOffset;

        struct Input
        {
            float2 uv_MaskTex;
            float3 worldPos;
            float3 texcoord : TEXCOORD0;
            fixed4 color : COLOR;
            float2 uv_RevealMask;
        };

        //edge stuff
        fixed4 _EdgeColorA;
        fixed4 _EdgeColorB;
        fixed _EdgeValue;
        fixed _EdgeWidth;
        //sampler2D _EdgeNoise;
        //float4 _EdgeNoise_ST;
        fixed2 _EdgeSpeed;

        //Reveal mask
        sampler2D _RevealMask;
        half _GeneralAlpha;
        half _AlphaMod;

        //BoundaryFade
        int _EnableDistanceFade;
        fixed _FadeRadius;
        fixed4 _FadePosition3D;
        fixed4 _FadeScaleFactor;
        fixed _FadeWidth;

        //DynamicDisplacement
        fixed _SubDisplace;

        //Specifc Property
        fixed2 _OceanSpeedA;
        fixed2 _OceanSpeedB;

        //texture A stuff
        sampler2D _AlbedoA;
        fixed _SmoothnessA;
        fixed4 _AlbedoA_ST;
        sampler2D _MetallicA;
        sampler2D _NormalA;
        sampler2D _OcclusionA;
        sampler2D _DisplacementA;

        fixed _DisplacementScaleA;
        fixed _DisplacementOffsetA;

        //texture B stuff
        sampler2D _AlbedoB;
        fixed _SmoothnessB;
        fixed4 _AlbedoB_ST;
        sampler2D _MetallicB;
        sampler2D _NormalB;
        sampler2D _OcclusionB;
        sampler2D _DisplacementB;

        fixed _DisplacementScaleB;
        fixed _DisplacementOffsetB;

        //texture C stuff
        sampler2D _AlbedoC;
        fixed4 _AlbedoC_ST;
        sampler2D _NormalC;
        sampler2D _OcclusionC;
        sampler2D _DisplacementC;

        //vertex blend
        float4 tex2Dlod_blend(sampler2D a, sampler2D b, sampler2D c, float4 texcoordA, float4 texcoordB, float4 texcoordC, float blendFactor, float blendfactorBC)
        {
            return lerp(tex2Dlod(a, texcoordA), lerp(tex2Dlod(b, texcoordB), tex2Dlod(c, texcoordC), blendfactorBC), blendFactor);
        }

        //3 Layers blend
        float4 tex2D_3LayerBlend(sampler2D a, sampler2D b, float4 c, float2 texcoordA, float2 texcoordB, float blendFactorAB, float blendFactorBC) {

            return lerp(tex2D(a, texcoordA), lerp(tex2D(b, texcoordB), c, blendFactorBC), blendFactorAB);
        }

        //spherical blend factor
        float getBlendFactorSpherical_local(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);

            float posy = (pos.y);
            float vy = (uv.y - posy);

            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            radius /= width;

            return  saturate((sphericalArea / radius - width));
        }

        //Vert modification 
        void vert(inout appdata_full v)
        {
            fixed blendFactor = tex2Dlod(_MaskTex, v.texcoord).r;
            blendFactor = saturate(blendFactor + _MaskOffset);

            //get the displacement factor and transform based on properties
            fixed4 texcoordA = float4(v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw, 1.0, 1.0);
            fixed4 texcoordB = float4(v.texcoord.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw, 1.0, 1.0);
            fixed4 texcoordC = float4(v.texcoord.xy * _AlbedoC_ST.xy + _AlbedoC_ST.zw, 1.0, 1.0);
            fixed thirdlayer = tex2Dlod(_AlbedoC, texcoordC).a;

            fixed displacementAmount = tex2Dlod_blend(_DisplacementA, _DisplacementB, _DisplacementC, texcoordA, texcoordB, texcoordC, blendFactor, thirdlayer);
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);

            fixed dynamicdisplace = 0;

            /*if (_EnableDistanceFade > 0) {
                fixed3 worldpos = mul(unity_ObjectToWorld, v.vertex);
                fixed CycleFactor = getBlendFactorSpherical_local_vert(_FadePosition3D.xz, 0.1, _FadeRadius * 1.5, worldpos.xz, _FadeScaleFactor);
                dynamicdisplace = lerp(_SubDisplace, 0.0, CycleFactor) * _FadePosition3D.y;
            }*/

            v.vertex += float4(0, displacementAmount + dynamicdisplace, 0, 0);
            v.color.xy = texcoordA.xy;
            v.color.zw = texcoordB.xy;

        }

        //main blend surface shading
        void surf_main(Input IN, inout SurfaceOutputStandard o, float blendFactor)
        {
            fixed2 texcoordC = float2(IN.uv_MaskTex.xy * _AlbedoC_ST.xy + _AlbedoC_ST.zw);
            fixed4 netAlbedo = tex2D(_AlbedoC, texcoordC);
            fixed4 netNormal = tex2D(_NormalC, texcoordC);
            fixed4 netOcclu = tex2D(_OcclusionC, texcoordC);
            fixed2 texcoordA = IN.color.xy;
            fixed2 texcoordB = IN.color.zw;
        
            #ifdef SLOTA_IS_EMPTY
            if (blendFactor < 0.5) discard;
            #endif

            #ifdef SLOTB_IS_EMPTY
            if (blendFactor > 0.5) discard;
            #endif

            fixed4 color;
            color = tex2D_3LayerBlend(_AlbedoA, _AlbedoB, netAlbedo, texcoordA, texcoordB, blendFactor, netAlbedo.a);
            o.Albedo = color.rgb;
            o.Normal = UnpackNormal(tex2D_3LayerBlend(_NormalA, _NormalB, netNormal, texcoordA, texcoordB, blendFactor, netAlbedo.a));
            o.Occlusion = tex2D_3LayerBlend(_OcclusionA, _OcclusionB, netOcclu, texcoordA, texcoordB, blendFactor, netAlbedo.a).r;
            fixed4 metalsmooth = tex2D_3LayerBlend(_MetallicA, _MetallicB, float4 (0, 0, 0, 0), texcoordA, texcoordB, blendFactor, netAlbedo.a);
            o.Smoothness = saturate(lerp(_SmoothnessA, _SmoothnessB, blendFactor)) * (metalsmooth.r);
            o.Metallic = 0;
            o.Alpha = color.a;

            //blend edge
            if (blendFactor < _EdgeValue) {
                blendFactor = 1.0 - saturate((_EdgeValue - blendFactor) / _EdgeWidth);
            }
            else {
                blendFactor = 1.0 - saturate((blendFactor - _EdgeValue) / _EdgeWidth);
            }

            fixed4 edgeCol = _EdgeColorA;
            o.Albedo = lerp(o.Albedo, edgeCol, blendFactor);
            o.Albedo *= o.Occlusion;
        }


        //end shading
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed blendFactor = tex2D(_MaskTex, IN.uv_MaskTex).r;
            blendFactor = saturate(blendFactor + _MaskOffset);

            surf_main(IN, o, blendFactor);

            fixed revealmasksample = tex2D(_RevealMask, IN.uv_RevealMask).r;
            fixed alphamodstep = lerp(0.0, _AlphaMod, revealmasksample);

            fixed revealAlpha = saturate((revealmasksample + alphamodstep) * _GeneralAlpha);

            fixed EdgeFadeAlpha = 1;

            if (_EnableDistanceFade > 0) {
                fixed CycleFactor = getBlendFactorSpherical_local(_FadePosition3D.xz, _FadeWidth, _FadeRadius, IN.worldPos.xz, _FadeScaleFactor);
                EdgeFadeAlpha = lerp(1.0, 0.0, CycleFactor);
            }
            o.Alpha *= EdgeFadeAlpha * revealAlpha;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
