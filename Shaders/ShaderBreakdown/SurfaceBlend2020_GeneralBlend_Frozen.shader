﻿Shader "HeightBlend/SurfaceBlend2020_GeneralBlend_transparent_Frozen"
{
     Properties
    {
        //Revealing
        [Header(Revealing)]
        _AlphaMod("Alpha modification",Range(-1.0,1.0)) = 0.0
        _GeneralAlpha("General Transparency", Range(0.0,2.0)) = 1.5
        _RevealMask("Revealing mask",2D) = "white" {}

        //DistanceFade
        [Space(10)]
        [Header(DistanceFade)]
        [Toggle] _EnableDistanceFade("Enable Distance Fade", int) = 0
        _FadeRadius("FadeRadius",Range(0.0,10.0)) = 0.0
        _FadePosition3D("Fade Center Position in 3D space",Vector) = (0,0,0,0)
        _FadeScaleFactor("Fade Scale Factor (XY)",Vector) = (1,1,0,0)
        _FadeWidth("FadeWidth",Range(0.1,20.0)) = 0.1

        //DynamicDisplacement
        [Space(10)]
        [Header(DynamicDisplacement)]
        _SubDisplace("Dynamic displacement Max",float) = 0.5

        //blendMask
        [Space(10)]
        [Header(Blending)]
        _MaskTex("Mask Texture", 2D) = "black" {}
        _MaskOffset("Mask Offset", Range(-1.0, 1.0)) = 0.0
        _MaskOffset2("Mask Offset to D", Range(-1.0,1.0)) = 0.0
        //blendEdge
        _EdgeColorA("Edge ColorA", Color) = (1, 0, 0, 1)
        //_EdgeColorB("Edge ColorB", Color) = (0, 1, 0, 1)
        _EdgeValue("Edge Value", Range(0, 1)) = 0.5
        _EdgeWidth("Edge Width", Range(0, 0.5)) = 0.5
        //_EdgeNoise("Edge Noise", 2D) = "white" {}
        _EdgeSpeed("Edge Speed", Vector) = (0.1, -0.08, 0, 0)

        //Specific property
        [Space(10)]
        [Header(OceanSpecific)]
        _WaterMask("Mask for Encouter Water", 2D) = "black" {}
        //[Normal] _WaterNormal("Normal for water effect", 2D) = "bump" {}
        [Normal] _UnhealthyWaterNormal("Normal for unhealthy water", 2D) = "bump" {}
        [Normal] _IceDetailNormal("Normal for Ice detail", 2D) = "bump" {}
        _NormalStrengh("overall normal strengh", Range(0,1.5)) = 1 
        _OceanSpeedA("Ocean Speed A", Vector) = (0.1, -0.13, 0, 0)
        _OceanSpeedB("Ocean Speed B", Vector) = (-0.09, 0.21, 0, 0)
        _DarkenSlider("Darken", Range(0.0,1.0)) = 0

        //[HideInInspector]
        _CPUTime("CPUtime", Float) = 0

        //Texture set A
        [Space(10)]
        [Header(Texture Set A)]
        _AlbedoA("Albedo A", 2D) = "white" {}
        [Normal] _NormalA("NormalA", 2D) = "bump" {}
        _PBRMapA("A PBR map set (r=AO, g=height, b=rough, a=metallic)", 2D) = "black" {}

        _SmoothnessA("SmoothnessA", Range(0.0, 1.0)) = 0.0
        _OcclusionSlider("Occlusion Strength", Range(0.0,1.0)) = 1.0
        
        _DisplacementScaleA("Displacement Scale A", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA("Displacement Offset A", Range(-1.0, 1.0)) = 0.0

        //Texture set B
        [Space(10)]
        [Header(Texture Set B)]
        _AlbedoB("Albedo B", 2D) = "white" {}
        [Normal] _NormalB("Normal B", 2D) = "bump" {}
        _PBRMapB("B PBR map set (r=AO, g=height, b=rough, a=metallic)", 2D) = "black" {}

        _SmoothnessB("SmoothnessA", Range(0.0, 1.0)) = 0.0

        _DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

        ////Texture set C
        //[Space(10)]
        //[Header(Texture Set C)]
        //_AlbedoC("Albedo C", 2D) = "black" {}
        //[Normal] _NormalC("Normal C", 2D) = "bump" {}
        //_OcclusionC("Occlusion C", 2D) = "white" {}
        //_DisplacementC("Displacement C", 2D) = "black"{}

        //_DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        //_DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

        //Texture set D
        [Space(10)]
        [Header(Texture Set D)]
        _UVRotationDegree("UV Rotation degree", float) = 60
        _AlbedoD("Albedo D", 2D) = "white" {}
        [Normal] _NormalD("Normal D", 2D) = "bump" {}
       _PBRMapD("D PBR map set (r=AO, g=height, b=rough, a=metallic)", 2D) = "black" {}

        _SmoothnessD("SmoothnessD", Range(0.0, 1.0)) = 0.0
        _WaterTransparency("The transparency dial for water", Range(0.0,1.0)) = 0.8
        _DisplacementScaleD("Displacement Scale D", Range(0.0, 10)) = 0.5
        _DisplacementOffsetD("Displacement Offset D", Range(-1.0, 1.0)) = 0.0

        
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard noforwardadd vertex:vert keepalpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.6

        #pragma shader_feature SLOTA_IS_EMPTY
        #pragma shader_feature SLOTB_IS_EMPTY

        static const float pi = 3.141592653589793238462;

        //blendMask stuff
        sampler2D _MaskTex;
        fixed _MaskOffset;

        fixed _MaskOffset2;

        struct Input
        {
            float2 uv_MaskTex;
            float3 worldPos;
            float3 texcoord : TEXCOORD0;
            fixed4 color : COLOR;
            fixed4 RotatedUV_D : TEXCOORD1;
            float2 uv_RevealMask;
        };

        //edge stuff
        fixed4 _EdgeColorA;
        fixed4 _EdgeColorB;
        fixed _EdgeValue;
        fixed _EdgeWidth;
        //sampler2D _EdgeNoise;
        //float4 _EdgeNoise_ST;
        fixed2 _EdgeSpeed;

        //Reveal mask
        sampler2D _RevealMask;
        half _GeneralAlpha;
        half _AlphaMod;

        //BoundaryFade
        int _EnableDistanceFade;
        fixed _FadeRadius;
        fixed4 _FadePosition3D;
        fixed4 _FadeScaleFactor;
        fixed _FadeWidth;

        //DynamicDisplacement
        fixed _SubDisplace;

        //Specifc Property
        sampler2D _WaterMask;
        //sampler2D _UnhealthyWaterMask;
        //fixed4 _UnhealthyWaterNormal_ST;
        //sampler2D _WaterNormal;
        //fixed4 _WaterNormal_ST;
        sampler2D _UnhealthyWaterNormal;
        fixed4 _UnhealthyWaterNormal_ST;
        sampler2D _IceDetailNormal;
        fixed4 _IceDetailNormal_ST;
        
        fixed _CPUTime;
        fixed _NormalStrengh;
        fixed2 _OceanSpeedA;
        fixed2 _OceanSpeedB;
        fixed _DarkenSlider;

        //texture A stuff
        sampler2D _AlbedoA;
        fixed _SmoothnessA;
        fixed4 _AlbedoA_ST;
        sampler2D _NormalA;
        fixed _OcclusionSlider;
        sampler2D _PBRMapA;

        fixed _DisplacementScaleA;
        fixed _DisplacementOffsetA;

        //texture B stuff
        sampler2D _AlbedoB;
        fixed _SmoothnessB;
        fixed4 _AlbedoB_ST;
        sampler2D _NormalB;
        sampler2D _PBRMapB;

        fixed _DisplacementScaleB;
        fixed _DisplacementOffsetB;

        //texture C stuff
        //sampler2D _AlbedoC;
        //fixed4 _AlbedoC_ST;
        //sampler2D _NormalC;
        //sampler2D _OcclusionC;
        //sampler2D _DisplacementC;

        //texture D stuff
        fixed _UVRotationDegree;
        sampler2D _AlbedoD;
        fixed _SmoothnessD;
        fixed4 _AlbedoD_ST;
        sampler2D _NormalD;
        sampler2D _PBRMapD;
        fixed _WaterTransparency;
        fixed _DisplacementScaleD;
        fixed _DisplacementOffsetD;

        // 2D Random
        float random(fixed2 st) {
            return frac(sin(dot(st.xy,
                fixed2(12.9898, 78.233)))
                * 43758.5453123);
        }

        float noise(fixed2 st) {
            fixed2 i = floor(st);
            fixed2 f = frac(st);

            // Four corners in 2D of a tile
            fixed a = random(i);
            fixed b = random(i + fixed2(1.0, 0.0));
            fixed c = random(i + fixed2(0.0, 1.0));
            fixed d = random(i + fixed2(1.0, 1.0));

            // Smooth Interpolation

            // Cubic Hermine Curve.  Same as SmoothStep()
            fixed2 u = f * f * (3.0 - 2.0 * f);
            // u = smoothstep(0.,1.,f);

            // Mix 4 coorners percentages
            return lerp(a, b, u.x) +
                (c - a) * u.y * (1.0 - u.x) +
                (d - b) * u.x * u.y;
        }

        fixed4 tex2Dlod_simpleblend(sampler2D a, sampler2D b, float4 texcoordA, float4 texcoordB, float blendFactor)
        {
            return lerp(tex2Dlod(a, texcoordA), tex2Dlod(b, texcoordB), blendFactor);
        }

        //vertex blend
        fixed4 tex2Dlod_blend(sampler2D a, sampler2D b, sampler2D c, float4 texcoordA, float4 texcoordB, float4 texcoordC, float blendFactor, float blendfactorBC)
        {
            return lerp(tex2Dlod(a, texcoordA), lerp(tex2Dlod(b, texcoordB), tex2Dlod(c, texcoordC), blendfactorBC), blendFactor);
        }

        fixed4 tex2D_2LayerBlend(float4 a, sampler2D b, float2 texcoordB, float blendFactorAB) {
        
            return lerp(a, tex2D(b, texcoordB), blendFactorAB);
        
        }

        //A to B Normal value
        fixed4 tex2D_2LayerBlend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float4 unhealthynormal, float blendFactorAB) {

            return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB) + unhealthynormal, blendFactorAB);

        }


        fixed4 tex2D_2LayerBlend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float blendFactorAB) {

            return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactorAB);

        }


        //3 Layers blend
        fixed4 tex2D_3LayerBlend(sampler2D a, sampler2D b, float4 c, float2 texcoordA, float2 texcoordB, float blendFactorAB, float blendFactorBC) {

            return lerp(tex2D(a, texcoordA), lerp(tex2D(b, texcoordB), c, blendFactorBC), blendFactorAB);
        }

        //Animated
        fixed4 tex2D_doubleAnimated(sampler2D tex, float2 texcoord, float2 speedA, float2 speedB)
        {
            
            fixed2 texcoordA = texcoord  + speedA/1000 * _CPUTime;
            //fixed2 texcoordA = texcoord + speedA/1000 * _Time.y;
            fixed2 texcoordB = texcoord  + speedB/1000 * _CPUTime;
            //fixed2 texcoordB = texcoord + speedB/1000 * _Time.y;
            fixed4 colA = tex2D(tex, texcoordA );
            fixed4 colB = tex2D(tex, texcoordB );
            colB = fixed4(colB.r, colB.g, colB.b, colB.r);
            fixed4 colmin = min(colA, colB);
            fixed4 colmax = max(colA, colB);

            //fixed4 collerp = lerp(colA, colB, clamp(abs(sin(_CPUTime * 0.2)), 0.4, 0.6));
            fixed4 collerp2 = lerp(colmin, colmax, noise(texcoord));


            return collerp2;
        }


        //UVRotation
        float2 GetRotatedUV(float degree, float2 UV) {
        
            float radian = pi * degree / 180;

            //_UVRotationDegree
            float sinX = sin(radian);
            float cosX = cos(radian);
            float sinY = sin(radian);
            float2x2 rotationMatrix = float2x2(cosX, -sinX, sinY, cosX);

            float2 rotatedUV = mul(UV - float2(0.5, 0.5) * _AlbedoD_ST.xy, rotationMatrix) + float2(0.5, 0.5)  ;
        
            return rotatedUV;
        }


        //spherical blend factor
        float getBlendFactorSpherical_local(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);

            float posy = (pos.y);
            float vy = (uv.y - posy);

            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            radius /= width;

            return  saturate((sphericalArea / radius - width));
        }

        //spherical blend factor for vertex
        float getBlendFactorSpherical_local_vert(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);



            float posy = (pos.y);
            float vy = (uv.y - posy);


            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            return  saturate(sphericalArea / radius);

        }

        //Vert modification 
        void vert(inout appdata_full v)
        {
            //(r = AO, g = height, b = rough, a = metallic)

            fixed blendFactor = tex2Dlod(_MaskTex, v.texcoord).r;
            blendFactor = saturate(blendFactor + _MaskOffset);
            
            fixed blendFactorAllToD = tex2Dlod(_MaskTex, v.texcoord).r;
            blendFactorAllToD = saturate(blendFactor + _MaskOffset2);

            //get the displacement factor and transform based on properties
            fixed4 texcoordA = fixed4(v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw, 1.0, 1.0);
            fixed4 texcoordB = fixed4(v.texcoord.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw, 1.0, 1.0);
            fixed4 texcoordD = fixed4(v.texcoord.xy * _AlbedoD_ST.xy , 1.0, 1.0);
            fixed4 RotatedTexcoordD = fixed4(GetRotatedUV(_UVRotationDegree,texcoordD.xy)+_AlbedoD_ST.zw,1.0,1.0);

            //g value in the pack is displacement
            fixed displacementAmount = tex2Dlod_simpleblend(_PBRMapA, _PBRMapB, texcoordA, texcoordB, blendFactor).g;
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);

            fixed Temp_displacement = displacementAmount;
            displacementAmount = lerp(Temp_displacement, tex2Dlod(_PBRMapD, RotatedTexcoordD).g, blendFactorAllToD);
            displacementAmount *= lerp(Temp_displacement, _DisplacementScaleD, blendFactorAllToD);
            displacementAmount += lerp(Temp_displacement, _DisplacementOffsetD, blendFactorAllToD);


            fixed dynamicdisplace = 0;
            fixed OutPutDisplacement = displacementAmount;

            if (_EnableDistanceFade > 0) {
                fixed3 worldpos = mul(unity_ObjectToWorld, v.vertex);
                fixed CycleFactor = getBlendFactorSpherical_local_vert(_FadePosition3D.xz, _FadeWidth, _FadeRadius * 5 , worldpos.xz, _FadeScaleFactor);
                dynamicdisplace = lerp(_SubDisplace, 0.0, CycleFactor);
                OutPutDisplacement = lerp(displacementAmount, 0.0, CycleFactor*(1 - _MaskOffset2)) + lerp(dynamicdisplace,0, saturate(_MaskOffset2));
            }

            
            v.vertex.y += OutPutDisplacement;
            v.color.xy = texcoordA.xy;
            v.color.zw = texcoordB.xy;
            //v.texcoord1.xy = RotatedTexcoordD.xy;
            
        }

        //main blend surface shading
        void surf_main(Input IN, inout SurfaceOutputStandard o, float blendFactor, float blendFactor2)
        {
            //(r = AO, g = height, b = rough, a = metallic)
            fixed2 texcoordA = IN.color.xy;
            fixed2 texcoordB = IN.color.zw;
            fixed2 texcoordD = fixed2(IN.uv_MaskTex.xy * _AlbedoD_ST.xy);
            texcoordD = GetRotatedUV(_UVRotationDegree, texcoordD) + _AlbedoD_ST.zw;
            fixed4 Asample = tex2D(_PBRMapA, texcoordA);
            fixed4 Bsample = tex2D(_PBRMapB, texcoordB);
            fixed4 Dsample = tex2D(_PBRMapD, texcoordD);
            
            //fixed2 texcoordWater = fixed2(IN.uv_MaskTex.xy * _WaterNormal_ST.xy + _WaterNormal_ST.zw);
            //fixed2 texcoordUnhealthyWaterMask = fixed2(IN.uv_MaskTex.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw);
            fixed2 texcoordUnhealtyWaterNormal = fixed2(IN.uv_MaskTex.xy * _UnhealthyWaterNormal_ST.xy + _UnhealthyWaterNormal_ST.zw);
            fixed2 texcoordIceDetail = fixed2(IN.uv_MaskTex.xy * _IceDetailNormal_ST.xy + _IceDetailNormal_ST.zw);
            fixed CloseOceanMaskSample = tex2D(_WaterMask, texcoordD).r;
            fixed4 WaterNormalAnimateSample = tex2D_doubleAnimated(_UnhealthyWaterNormal, texcoordUnhealtyWaterNormal, _OceanSpeedA, _OceanSpeedB);
            fixed4 CloseOceanNormalSample = (WaterNormalAnimateSample) *CloseOceanMaskSample;
            fixed4 UnhealthyWaterNormalSampler = WaterNormalAnimateSample * Bsample.b;
            fixed4 IceDetailNormalSample = tex2D(_IceDetailNormal, texcoordIceDetail) * (1 - CloseOceanMaskSample);

            
        
            /*#ifdef SLOTA_IS_EMPTY
            if (blendFactor < 0.5) discard;
            #endif

            #ifdef SLOTB_IS_EMPTY
            if (blendFactor > 0.5) discard;
            #endif*/

            

            //Albedo;
            fixed4 color;
            fixed4 color2;
            color = tex2D_2LayerBlend(_AlbedoA, _AlbedoB, texcoordA, texcoordB, blendFactor);
            color2 = tex2D_2LayerBlend(color, _AlbedoD, texcoordD, blendFactor2);
            o.Albedo = color2.rgb;

            //Normal
            fixed4 Avalue = tex2D(_NormalA, texcoordA);
            fixed4 Bvalue = tex2D(_NormalB, texcoordB) * (1 - saturate(Bsample.b -0.4)) + UnhealthyWaterNormalSampler;
            fixed4 temp_normal = lerp(Avalue, Bvalue, blendFactor);
            fixed4 D_normal = tex2D(_NormalD, texcoordD) * (1-CloseOceanMaskSample) + CloseOceanNormalSample + IceDetailNormalSample;
            o.Normal = UnpackNormal( lerp(temp_normal, D_normal, blendFactor2));

            //Occlusion = r
            fixed AO_A = lerp(1, Asample.r ,  _OcclusionSlider);
            fixed temp_Occlusion = lerp(AO_A, Bsample.r, blendFactor);
            fixed Docclusion = Dsample.r;
            o.Occlusion = lerp(temp_Occlusion, Docclusion, blendFactor2);

            //metallic = a
            //fixed4 metalsmooth = tex2D_2LayerBlend(_PBRMapA, _PBRMapB, texcoordA, texcoordB, blendFactor).a;
            //fixed4 metalsmooth_3 = tex2D_2LayerBlend(metalsmooth, _PBRMapD, texcoordD, blendFactor2).a;


            //Smoothness = b 
            fixed roughnessAB = lerp(Asample.b, Bsample.b, blendFactor);
            fixed roughnessD = Dsample.b;
            fixed roughnessfilter = lerp(roughnessAB, roughnessD, blendFactor2);
            fixed temp_smoothvalue = lerp(_SmoothnessA, _SmoothnessB, blendFactor);
            o.Smoothness = lerp(temp_smoothvalue, _SmoothnessD, blendFactor2) *  (roughnessfilter);
            o.Metallic = 0;


            //Alpha in Albedo
            o.Alpha = color.a;
            o.Alpha *= lerp(1.0, saturate(1 - CloseOceanMaskSample + _WaterTransparency), blendFactor2);

            //blend edge
            if (blendFactor < _EdgeValue) {
                blendFactor = 1.0 - saturate((_EdgeValue - blendFactor) / _EdgeWidth);
            }
            else {
                blendFactor = 1.0 - saturate((blendFactor - _EdgeValue) / _EdgeWidth);
            }

            ////blend edge 2
            //if (blendFactor2 < _EdgeValue) {
            //    blendFactor2 = 1.0 - saturate((_EdgeValue - blendFactor2) / _EdgeWidth);
            //}
            //else {
            //    blendFactor2 = 1.0 - saturate((blendFactor2 - _EdgeValue) / _EdgeWidth);
            //}

            fixed4 edgeCol = _EdgeColorA;
            //o.Albedo = lerp(lerp(o.Albedo, edgeCol, blendFactor), edgeCol, blendFactor2);
            o.Albedo = lerp(o.Albedo, edgeCol, blendFactor);
            //o.Albedo *= o.Occlusion;
        }



        //end shading
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed MaskSample = tex2D(_MaskTex, IN.uv_MaskTex).r;
            fixed blendFactor = saturate(MaskSample + _MaskOffset);

            //fixed blendFactor = IN.factors.x;

            fixed blendFactorAllToD = saturate(MaskSample + _MaskOffset2);

            //fixed blendFactorAllToD = IN.factors.y;

            

            surf_main(IN, o, blendFactor, blendFactorAllToD);
            


            fixed revealmasksample = tex2D(_RevealMask, IN.uv_RevealMask).r;
            fixed alphamodstep = lerp(0.0, _AlphaMod, revealmasksample);

            fixed revealAlpha = saturate((revealmasksample + alphamodstep) * _GeneralAlpha);

            fixed EdgeFadeAlpha = 1;

            if (_EnableDistanceFade > 0) {
                fixed CycleFactor = getBlendFactorSpherical_local(_FadePosition3D.xz, _FadeWidth, _FadeRadius, IN.worldPos.xz, _FadeScaleFactor);
                EdgeFadeAlpha = lerp(1.0, 0.0, CycleFactor);
            }
            o.Metallic = _DarkenSlider;
            o.Alpha *= EdgeFadeAlpha * revealAlpha;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
