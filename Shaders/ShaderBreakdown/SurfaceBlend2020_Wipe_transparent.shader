﻿Shader "HeightBlend/SurfaceBlend2020_Wipe_transparent"
{
    Properties
    {
        //Revealing
        [Header(Revealing)]
        _AlphaMod("Alpha modification",Range(-1.0,1.0)) = 0.0
        _GeneralAlpha("General Transparency", Range(0.0,2.0)) = 1.5
        _RevealMask("Revealing mask",2D) = "white" {}

        //DistanceFade
        [Space(10)]
        [Header(DistanceFade)]
        [Toggle] _EnableDistanceFade("Enable Distance Fade", int) = 0
        _FadeRadius("FadeRadius",Range(0.0,10.0)) = 0.0
        _FadePosition3D("Fade Center Position in 3D space",Vector) = (0,0,0,0)
        _FadeScaleFactor("Fade Scale Factor (XY)",Vector) = (1,1,0,0)
        _FadeWidth("FadeWidth",Range(0.1,20.0)) = 0.1

       
        //DynamicDisplacement
        [Space(10)]
        [Header(DynamicDisplacement)]
        _SubDisplace("Dynamic displacement Max",float) = 0.5

        //wipe 
        [Space(10)]
        [Header(Wipe)]
        [Toggle] _EnableFreeDirection("Enable Free wipe direction", int) = 0
        _WipePosition("Wipe Position", Range(-3.0, 2.0)) = 0.5
        _WipeWidth("Wipe Width", Range(0.0, 1.0)) = 0.1
        [Enum(Horizontal, 0, Vertical, 1)] _WipeDirection("Wipe Direction", int) = 0
        _WipeRotationDegree("Rotation degree", float) = 60
        _WipeOffset("Wipe Offset", float) = 0


        //Specific property
        [Space(10)]
        [Header(OceanSpecific)]
        _OceanSpeedA("Ocean Speed A", Vector) = (0.1, -0.13, 0, 0)
        _OceanSpeedB("Ocean Speed B", Vector) = (-0.09, 0.21, 0, 0)
        _DarkenSlider("Darken", Range(0.0,1.0)) = 0

        //Texture set A
        [Space(10)]
        [Header(Texture Set A)]
        _AlbedoA("Albedo A", 2D) = "white" {}
        _SmoothnessA("SmoothnessA", Range(0.0, 1.0)) = 1.0
        [Normal] _NormalA("NormalA", 2D) = "bump" {}
        _PBRMapA("A PBR map set (r=AO, g=height, b=rough, a=metallic)", 2D) = "black" {}

        _DisplacementScaleA("Displacement Scale A", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA("Displacement Offset A", Range(-1.0, 1.0)) = 0.0

        //Texture set B
        [Space(10)]
        [Header(Texture Set B)]
        _AlbedoB("Albedo B", 2D) = "white" {}
        _SmoothnessB("SmoothnessA", Range(0.0, 1.0)) = 1.0
        [Normal] _NormalB("Normal B", 2D) = "bump" {}
        _PBRMapB("B PBR map set (r=AO, g=height, b=rough, a=metallic)", 2D) = "black" {}

        _DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

        //Texture set C
       /* [Space(10)]
        [Header(Texture Set C)]
        _AlbedoC("Albedo C", 2D) = "black" {}
        [Normal] _NormalC("Normal C", 2D) = "bump" {}
        _OcclusionC("Occlusion C", 2D) = "white" {}
        _DisplacementC("Displacement C", 2D) = "black"{}

        _DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0*/


    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard noforwardadd vertex:vert keepalpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.0

        #pragma shader_feature SLOTA_IS_EMPTY
        #pragma shader_feature SLOTB_IS_EMPTY
        #pragma shader_feature A_Is_Ocean
        #pragma shader_feature B_Is_Ocean

        

        struct Input
        {
            
            float3 worldPos;
            float3 texcoord : TEXCOORD0;
            fixed4 color : COLOR;
            float2 uv_RevealMask;

        };

        //wipe stuff
        int _EnableFreeDirection;
        fixed _WipePosition;
        fixed _WipeWidth;
        int _WipeDirection;
        fixed _WipeOffset;
        fixed _WipeRotationDegree;

        //Reveal mask
        sampler2D _RevealMask;
        half _GeneralAlpha;
        half _AlphaMod;

        //BoundaryFade
        int _EnableDistanceFade;
        fixed _FadeRadius;
        fixed4 _FadePosition3D;
        fixed4 _FadeScaleFactor;
        fixed _FadeWidth;

        //DynamicDisplacement
        fixed _SubDisplace;

        //Specifc Property
        fixed2 _OceanSpeedA;
        fixed2 _OceanSpeedB;
        fixed _DarkenSlider;

        //texture A stuff
        sampler2D _AlbedoA;
        fixed _SmoothnessA;
        fixed4 _AlbedoA_ST;
        sampler2D _NormalA;
        sampler2D _PBRMapA;

        fixed _DisplacementScaleA;
        fixed _DisplacementOffsetA;

        //texture B stuff
        sampler2D _AlbedoB;
        fixed _SmoothnessB;
        fixed4 _AlbedoB_ST;
        sampler2D _NormalB;
        sampler2D _PBRMapB;

        fixed _DisplacementScaleB;
        fixed _DisplacementOffsetB;

        //texture C stuff
        //sampler2D _AlbedoC;
        //fixed4 _AlbedoC_ST;
        //sampler2D _NormalC;
        //sampler2D _OcclusionC;
        //sampler2D _DisplacementC;

        //shader functions

        //Wipe Blend function
        float4 tex2D_blend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float blendFactor)
        {

            #ifdef B_Is_Ocean
            float2 oceanuv1 = texcoordB + float2(0.01123, -0.0085) * _Time.y;
            float2 oceanuv2 = texcoordB + float2(-0.00961, 0.0155321) * _Time.y;
            float4 ocean1 = tex2D(b, oceanuv1);
            float4 ocean2 = tex2D(b, oceanuv2);
            float4 ocean = min(ocean1, ocean2);

            return lerp(tex2D(a, texcoordA), ocean, blendFactor);

            #elif A_Is_Ocean
            float2 oceanuv1 = texcoordA + float2(0.01123, -0.0085) * _Time.y;
            float2 oceanuv2 = texcoordA + float2(-0.00961, 0.0155321) * _Time.y;
            float4 ocean1 = tex2D(a, oceanuv1);
            float4 ocean2 = tex2D(a, oceanuv2);
            float4 ocean = min(ocean1, ocean2);

            return lerp(ocean, tex2D(b, texcoordB), blendFactor);

            #else

            return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactor);
            #endif

        }

        //Triple texture blending
        float4 tex2D_3LayerBlend_noocean(sampler2D a, sampler2D b, float4 c, float2 texcoordA, float2 texcoordB, float blendFactorAB, float blendFactorBC) {

            return lerp(tex2D(a, texcoordA), lerp(tex2D(b, texcoordB), c, blendFactorBC), blendFactorAB);
        }

        //Wipe vertex blend
        float4 tex2Dlod_blend(sampler2D a, sampler2D b, sampler2D c, float4 texcoordA, float4 texcoordB, float4 texcoordC, float blendFactor, float blendfactorBC)
        {

            #ifdef B_Is_Ocean
            float4 oceanuv1 = texcoordB + float4(0.01123, -0.0085, 0, 0) * _Time.y;
            float4 oceanuv2 = texcoordB + float4(-0.00961, 0.0155321, 0, 0) * _Time.y;
            float4 ocean1 = tex2Dlod(b, oceanuv1);
            float4 ocean2 = tex2Dlod(b, oceanuv2);
            float4 ocean = min(ocean1, ocean2);

            return lerp(tex2Dlod(a, texcoordA), ocean, blendFactor);

            #elif A_Is_Ocean
            float4 oceanuv1 = texcoordA + float4(0.01123, -0.0085, 0, 0) * _Time.y;
            float4 oceanuv2 = texcoordA + float4(-0.00961, 0.0155321, 0, 0) * _Time.y;
            float4 ocean1 = tex2Dlod(a, oceanuv1);
            float4 ocean2 = tex2Dlod(a, oceanuv2);
            float4 ocean = min(ocean1, ocean2);

            return lerp(ocean, tex2Dlod(b, texcoordB), blendFactor);

            #else
            return lerp(tex2Dlod(a, texcoordA), lerp(tex2Dlod(b, texcoordB), tex2Dlod(c, texcoordC), blendfactorBC), blendFactor);

            #endif
        }

        //spherical blend factor
        float getBlendFactorSpherical_local(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);



            float posy = (pos.y);
            float vy = (uv.y - posy);


            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            radius /= width;

            return  saturate((sphericalArea / radius - width));

        }

        //Dynamic Direction
        float getFreeBlendFactor(float pos, float size, float offset, float2 uv)
        {
           
            float sinX = sin(_WipeRotationDegree);
            float cosX = cos(_WipeRotationDegree);
            float sinY = sin(_WipeRotationDegree);
            float2x2 rotationMatrix = float2x2(cosX, -sinX, sinY, cosX);

            float2 rotatedUV = mul((uv - float2(0.5, 0.5)), rotationMatrix);

            float x = ((rotatedUV.x + offset) % 1.0) - pos;
            x += size;
            float y = ((rotatedUV.y + offset) % 1.0) - pos;
            y += size;
            float relativePos = x + y;

            return 1.0 - saturate(relativePos / (size * 2.0));

        }

        //Still direction
        float getBlendFactor(float pos, float size, int direction, float offset, float2 uv)
        {
            if (direction <= 0) {
                float relativePos = ((uv.x + offset) % 1.0) - pos;
                relativePos += size;
                return 1.0 - saturate(relativePos / (size * 2.0));
            }
            else {
                float relativePos = ((uv.y + offset) % 1.0) - pos;
                relativePos += size;
                return 1.0 - saturate(relativePos / (size * 2.0));
            }

        }


        void vert(inout appdata_full v)
        {

            fixed blendFactor = 0;
            if (_EnableFreeDirection > 0)
            {
                //Free Direction
                blendFactor = getFreeBlendFactor(_WipePosition, _WipeWidth, _WipeOffset, v.texcoord.xy);
            }
            else {
                //X,Y still direction
                blendFactor = getBlendFactor(_WipePosition, _WipeWidth, _WipeDirection, _WipeOffset, v.texcoord.xy);
            }


            //get the displacement factor and transform based on properties
            fixed4 texcoordA = float4(v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw, 1.0, 1.0);
            fixed4 texcoordB = float4(v.texcoord.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw, 1.0, 1.0);
            fixed Asample = tex2Dlod(_PBRMapA, texcoordA).g;
            fixed Bsample = tex2Dlod(_PBRMapB, texcoordB).g;

            fixed displacementAmount = lerp(Asample, Bsample, blendFactor);;
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);

            fixed dynamicdisplace = 0;

            /*if (_EnableDistanceFade > 0) {
                fixed3 worldpos = mul(unity_ObjectToWorld, v.vertex);
                fixed CycleFactor = getBlendFactorSpherical_local_vert(_FadePosition3D.xz, 0.1, _FadeRadius * 1.5, worldpos.xz, _FadeScaleFactor);
                dynamicdisplace = lerp(_SubDisplace, 0.0, CycleFactor) * _FadePosition3D.y;
            }*/

            v.vertex += float4(0, displacementAmount + dynamicdisplace, 0, 0);
            v.color.xy = texcoordA.xy;
            v.color.zw = texcoordB.xy;

        }

        void surf_main(Input IN, inout SurfaceOutputStandard o, float blendFactor)
        {
            //fixed2 texcoordC = fixed2(IN.uv_RevealMask.xy * _AlbedoC_ST.xy + _AlbedoC_ST.zw);
            //fixed4 netAlbedo = tex2D(_AlbedoC, texcoordC);
            //fixed4 netNormal = tex2D(_NormalC, texcoordC);
            //fixed4 netOcclu = tex2D(_OcclusionC, texcoordC);
            fixed2 texcoordA = IN.color.xy;
            fixed2 texcoordB = IN.color.zw;
            fixed4 Asample = tex2D(_PBRMapA, texcoordA);
            fixed4 Bsample = tex2D(_PBRMapB, texcoordB);

            //Empty slot pixel discarding
            #ifdef SLOTA_IS_EMPTY
            if (blendFactor < 0.5) discard;
            #endif

            #ifdef SLOTB_IS_EMPTY
            if (blendFactor > 0.5) discard;
            #endif

            fixed4 color;
            color = tex2D_blend(_AlbedoA, _AlbedoB, texcoordA, texcoordB, blendFactor);
            o.Albedo = color.rgb;
            o.Normal = UnpackNormal(tex2D_blend(_NormalA, _NormalB, texcoordA, texcoordB, blendFactor));
            o.Occlusion = lerp(Asample.r, Bsample.r, blendFactor);
            fixed roughnessAB = lerp(Asample.b, Bsample.b, blendFactor);
            fixed temp_smoothvalue = lerp(_SmoothnessA, _SmoothnessB, blendFactor);

            //if B slot is ocean, doing ocean animation on B
            #ifdef B_Is_Ocean
            o.Smoothness = lerp(Asample.b, 0.8, blendFactor);

            //if A slot is ocean, doing ocean animation on A
            #elif A_Is_Ocean
            o.Smoothness = lerp(0.8, Bsample.b, blendFactor);

            //normal texture blending
            #else
            o.Smoothness = saturate(lerp((1-_SmoothnessA), (1-_SmoothnessB), blendFactor) * roughnessAB);
            #endif

            //o.Metallic = tex2D_blend(_MetallicA, _MetallicB, texcoordA, texcoordB, blendFactor);
            o.Alpha = color.a;

            o.Albedo *= o.Occlusion;

        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float blendFactor = 0;

            if (_EnableFreeDirection > 0)
            {
                //Free Direction
                blendFactor = getFreeBlendFactor(_WipePosition, _WipeWidth, _WipeOffset, IN.uv_RevealMask);
            }
            else {
                //X,Y still direction
                blendFactor = getBlendFactor(_WipePosition, _WipeWidth, _WipeDirection, _WipeOffset, IN.uv_RevealMask);
            }

            surf_main(IN, o, blendFactor);

            fixed revealmasksample = tex2D(_RevealMask, IN.uv_RevealMask).r;
            fixed alphamodstep = lerp(0.0, _AlphaMod, revealmasksample);
            fixed revealAlpha = saturate((revealmasksample + alphamodstep) * _GeneralAlpha);
            fixed EdgeFadeAlpha = 1;

            if (_EnableDistanceFade > 0) {
                fixed CycleFactor = getBlendFactorSpherical_local(_FadePosition3D.xz, _FadeWidth, _FadeRadius, IN.worldPos.xz, _FadeScaleFactor);
                EdgeFadeAlpha = lerp(1.0, 0.0, CycleFactor);
            }
            //o.Metallic *= _DarkenSlider;
            o.Alpha *= EdgeFadeAlpha * revealAlpha;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
