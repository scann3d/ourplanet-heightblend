Shader "HeightBlend/SurfaceBlend2020_GeneralBlend_transparent_Forest"
{
     Properties
    {
        //Revealing
        [Header(Revealing)]
        _AlphaMod("Alpha modification",Range(-1.0,1.0)) = 0.0
        _GeneralAlpha("General Transparency", Range(0.0,2.0)) = 1.5
        _RevealMask("Revealing mask",2D) = "white" {}

        //DistanceFade
        [Space(10)]
        [Header(DistanceFade)]
        [Toggle] _EnableDistanceFade("Enable Distance Fade", int) = 0
        _FadeRadius("FadeRadius",Range(0.0,10.0)) = 0.0
        _FadePosition3D("Fade Center Position in 3D space",Vector) = (0,0,0,0)
        _FadeScaleFactor("Fade Scale Factor (XY)",Vector) = (1,1,0,0)
        _FadeWidth("FadeWidth",Range(0.1,20.0)) = 0.1

        //DynamicDisplacement
        [Space(10)]
        [Header(DynamicDisplacement)]
        _SubDisplace("Dynamic displacement Max",float) = 0

        //blendMask
        [Space(10)]
        [Header(Blending)]
        _MaskTex("Mask Texture", 2D) = "black" {}
        _MaskOffset("Healthy to Unhealthy", Range(-1.0, 1.0)) = 0.0
        //_NoCircleOffset("Healthy to rewilded", Range(0,1)) = 0.0
        //_MaskOffset2("All to Rewilded Healthy", Range(-1.0,1.0)) = 0.0
        //blendEdge
        _EdgeColorA("Edge ColorA", Color) = (1, 0, 0, 1)
        //_EdgeColorB("Edge ColorB", Color) = (0, 1, 0, 1)
        _EdgeValue("Edge Value", Range(0, 1)) = 0.5
        _EdgeWidth("Edge Width", Range(0, 0.5)) = 0.5
        _EdgeNoise("Edge Noise", 2D) = "white" {}
        //_EdgeSpeed("Edge Speed", Vector) = (0.1, -0.08, 0, 0)

        //Specific property
        [Space(10)]
        [Header(Additional)]
        
        
        _VFXMask("Mask for VFX(r= GoodWatermask, g = cloudmask, b= BadWatermask)", 2D) = "black" {}
        [Normal] _RewildedWaterNormal("Normal for rewilded water ", 2D) = "bump" {}
        _UVRotationDegree("UV Rotation degree of Healthy dirt", float) = 0
        //_DirtTexture("Texture for dirt detail, rgb", 2D) = "black"{}

        _CloudSpeed("CloudSpeed xy = cloud1, zw = cloud2", Vector) = (0.1, -0.13, -0.09, 0.21)
        _CloudOffset("Cloud Opacity offset", Range(-1,1)) = 0
        _OceanSpeedA("Water Speed A", Vector) = (0.1, -0.13, 0, 0)
        _OceanSpeedB("Water Speed B", Vector) = (-0.09, 0.21, 0, 0)
        
        //_WaterTransparency("The transparency dial for water", Range(0.0,1.0)) = 0.8
        _DarkenSlider("Darken(for transition only)", Range(0.0,1.0)) = 0
        _OverallDisplacement("Overall Displacement" , Range(0.0, 10)) = 0.0
        _OverallAO("Overal AO", Range(-1.0,1.0)) = 0.0
        //[HideInInspector]
        _CPUTime("CPUtime", Float) = 0

        //Texture set A
        [Space(10)]
        [Header(Texture Set A Healthy1)]
        _TintA("AlbedoA tint",Color) = (1,1,1,1)
        _AlbedoA("Albedo A", 2D) = "white" {}
        _SaturationA("Color Saturation of AlbedoA",Range(1.0,5.0)) = 1.0
        //_AlbedoD("Albedo A2", 2D) = "white" {}
        _WaterColor("Color of Healthy river", Color) = (0,0,0,0)
        [NoScaleOffset][Normal] _NormalA("NormalA", 2D) = "bump" {}
        
        //[NoScaleOffset][Normal] _NormalD("NormalA2", 2D) = "bump" {}
        [NoScaleOffset]_PBRMapA("A PBR map set (r=AO, g=height, b=rough, a=overallDisplacement)", 2D) = "black" {}
        //[NoScaleOffset]_PBRMapD("A2 PBR map set (r=AO, g=height, b=rough, a=additional_notinuse)", 2D) = "black" {}

        _SmoothnessA("SmoothnessA", Range(0.0, 1.0)) = 0.0
        //_SmoothnessD("SmoothnessA2", Range(0.0, 1.0)) = 0.0
        _OcclusionSlider("Ambient occlusion of A", Range(0.0,1.0)) = 1.0
        _WaterDisplacement("River Displacement", Range(0.9,1.0)) = 0.0
        _DisplacementScaleA("Displacement Scale A", Range(0.0, 10)) = 0.5
        //_DisplacementScaleD("Displacement Scale A2", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA("Displacement Offset A", Range(-3.0, 1.0)) = 0.0

        
        //_DisplacementOffsetD("Displacement Offset A2", Range(-3.0, 1.0)) = 0.0

        //Texture set B
        [Space(10)]
        [Header(Texture Set B Unhealthy)]
        _TintB("AlbedoB tint",Color) = (1,1,1,1)
        [NoScaleOffset]_AlbedoB("Albedo B", 2D) = "white" {}
        _SaturationB("Color Saturation of AlbedoB",Range(1.0,5.0)) = 1.0
        _WaterColor2("Color of Unhealthy river", Color) = (0,0,0,0)
        [NoScaleOffset][Normal] _NormalB("Normal B", 2D) = "bump" {}
        [NoScaleOffset]_PBRMapB("B PBR map set (r=AO, g=lDisplacement, b=rough, a=additional)", 2D) = "black" {}

        _SmoothnessB("SmoothnessB", Range(0.0, 1.0)) = 0.0
        _AoB("Ambient occlusion of B", Range(0.0,1.0)) = 0.5
        _RiverDisplacementOffset("Detail displacement offset", Range(0.6, 1.0)) = 0.9
        _DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB("Displacement Offset B", Range(-3.0, 1.0)) = 0.0

        ////Texture set C
        //[Space(10)]
        //[Header(Texture Set C)]
        //_AlbedoC("Albedo C", 2D) = "black" {}
        //[Normal] _NormalC("Normal C", 2D) = "bump" {}
        //_OcclusionC("Occlusion C", 2D) = "white" {}
        //_DisplacementC("Displacement C", 2D) = "black"{}

        //_DisplacementScaleB("Displacement Scale B", Range(0.0, 10)) = 0.5
        //_DisplacementOffsetB("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

        //Texture set D
        //[Space(10)]
        //[Header(Texture Set D Healthy2)]
        
        
        
       

        
        
        

        
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard noforwardadd vertex:vert keepalpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.6

        #pragma shader_feature SLOTA_IS_EMPTY
        #pragma shader_feature SLOTB_IS_EMPTY

        static const float pi = 3.141592653589793238462;

        //blendMask stuff
        sampler2D _MaskTex;
        fixed _MaskOffset;
        //fixed _NoCircleOffset;
        //fixed _MaskOffset2;

        struct Input
        {
            float2 uv_MaskTex;
            float3 worldPos;
            float3 texcoord : TEXCOORD0;
            fixed4 color : COLOR;
            fixed4 RotatedUV_D : TEXCOORD1;
            float2 uv_RevealMask;
        };

        //edge stuff
        fixed4 _EdgeColorA;
        fixed4 _EdgeColorB;
        fixed _EdgeValue;
        fixed _EdgeWidth;
        sampler2D _EdgeNoise;
        float4 _EdgeNoise_ST;
        //fixed2 _EdgeSpeed;

        //Reveal mask
        sampler2D _RevealMask;
        half _GeneralAlpha;
        half _AlphaMod;

        //BoundaryFade
        int _EnableDistanceFade;
        fixed _FadeRadius;
        fixed4 _FadePosition3D;
        fixed4 _FadeScaleFactor;
        fixed _FadeWidth;

        //DynamicDisplacement
        fixed _SubDisplace;

        //Specifc Property
        fixed4 _WaterColor;
        fixed4 _WaterColor2;
        sampler2D _VFXMask;
        fixed4 _VFXMask_ST;
        //sampler2D _UnhealthyWaterMask;
        //fixed4 _UnhealthyWaterNormal_ST;
        //sampler2D _WaterNormal;
        //fixed4 _WaterNormal_ST;
        sampler2D _RewildedWaterNormal;
        fixed4 _RewildedWaterNormal_ST;

\
        
        fixed _CPUTime;
        //fixed _NormalStrengh;
        fixed4 _CloudSpeed;
        fixed _CloudOffset;
        fixed2 _OceanSpeedA;
        fixed2 _OceanSpeedB;
        fixed _DarkenSlider;
        fixed _OverallDisplacement;
        fixed _OverallAO;
        fixed _WaterDisplacement;

        //texture A stuff
        fixed4 _TintA;
        sampler2D _AlbedoA;
        fixed _SaturationA;
        fixed _SmoothnessA;
        fixed4 _AlbedoA_ST;
        sampler2D _NormalA;
        fixed _OcclusionSlider;
        sampler2D _PBRMapA;

        fixed _DisplacementScaleA;
        fixed _DisplacementOffsetA;

        //texture B stuff
        fixed4 _TintB;
        sampler2D _AlbedoB;
        fixed _SaturationB;
        fixed _SmoothnessB;
        fixed _AoB;
        //fixed4 _AlbedoB_ST;
        sampler2D _NormalB;
        sampler2D _PBRMapB;

        fixed _DisplacementScaleB;
        fixed _DisplacementOffsetB;

        //texture C stuff
        //sampler2D _AlbedoC;
        //fixed4 _AlbedoC_ST;
        //sampler2D _NormalC;
        //sampler2D _OcclusionC;
        //sampler2D _DisplacementC;

        //texture D stuff
        fixed _UVRotationDegree;
        //sampler2D _AlbedoD;
        //fixed _SmoothnessD;
        //fixed4 _AlbedoD_ST;
        //sampler2D _NormalD;
        //sampler2D _PBRMapD;
        //fixed _WaterTransparency;
        fixed _RiverDisplacementOffset;
        //fixed _DisplacementScaleD;
        //fixed _DisplacementOffsetD;

        // 2D Random
        float random(fixed2 st) {
            return frac(sin(dot(st.xy,
                fixed2(12.9898, 78.233)))
                * 43758.5453123);
        }

        float noise(fixed2 st) {
            fixed2 i = floor(st);
            fixed2 f = frac(st);

            // Four corners in 2D of a tile
            fixed a = random(i);
            fixed b = random(i + fixed2(1.0, 0.0));
            fixed c = random(i + fixed2(0.0, 1.0));
            fixed d = random(i + fixed2(1.0, 1.0));

            // Smooth Interpolation

            // Cubic Hermine Curve.  Same as SmoothStep()
            fixed2 u = f * f * (3.0 - 2.0 * f);
            // u = smoothstep(0.,1.,f);

            // Mix 4 coorners percentages
            return lerp(a, b, u.x) +
                (c - a) * u.y * (1.0 - u.x) +
                (d - b) * u.x * u.y;
        }


        //Color format
        float3 RGBToHSV(float3 c)
        {
            float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
            float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
            float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));
            float d = q.x - min(q.w, q.y);
            float e = 1.0e-10;
            return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
        }

        float3 HSVToRGB(float3 c)
        {
            float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
            float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
            return c.z * lerp(K.xxx, saturate(p - K.xxx), c.y);
        }

        float3 Saturation(float3 p, float v)
        {
            p = RGBToHSV(p);
            p.y *= v;
            return HSVToRGB(p);
        }

        fixed4 tex2Dlod_simpleblend(sampler2D a, sampler2D b, float4 texcoordA, float4 texcoordB, float blendFactor)
        {
            return lerp(tex2Dlod(a, texcoordA), tex2Dlod(b, texcoordB), blendFactor);
        }

        //vertex blend
        fixed4 tex2Dlod_blend(sampler2D a, sampler2D b, sampler2D c, float4 texcoordA, float4 texcoordB, float4 texcoordC, float blendFactor, float blendfactorBC)
        {
            return lerp(tex2Dlod(a, texcoordA), lerp(tex2Dlod(b, texcoordB), tex2Dlod(c, texcoordC), blendfactorBC), blendFactor);
        }

        fixed4 tex2D_2LayerBlend(float4 a, sampler2D b, float2 texcoordB, float blendFactorAB) {
        
            return lerp(a, tex2D(b, texcoordB), blendFactorAB);
        
        }

        //A to B Normal value
        fixed4 tex2D_2LayerBlend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float4 unhealthynormal, float blendFactorAB) {

            return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB) + unhealthynormal, blendFactorAB);

        }


        fixed4 tex2D_2LayerBlend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float blendFactorAB) {

            return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactorAB);

        }


        //3 Layers blend
        fixed4 tex2D_3LayerBlend(sampler2D a, sampler2D b, float4 c, float2 texcoordA, float2 texcoordB, float blendFactorAB, float blendFactorBC) {

            return lerp(tex2D(a, texcoordA), lerp(tex2D(b, texcoordB), c, blendFactorBC), blendFactorAB);
        }

        //Animated
        fixed4 tex2D_doubleAnimated(sampler2D tex, float2 texcoord, float2 speedA, float2 speedB)
        {
            
            //fixed2 texcoordA = texcoord  + speedA/1000 * _CPUTime;
            fixed2 texcoordA = texcoord + speedA/1000 * _Time.y;
            //fixed2 texcoordB = texcoord  + speedB/1000 * _CPUTime;
            fixed2 texcoordB = texcoord + speedB/1000 * _Time.y;
            fixed4 colA = tex2D(tex, texcoordA );
            fixed4 colB = tex2D(tex, texcoordB );
            colB = fixed4(colB.r, colB.g, colB.b, colB.r);
            fixed4 colmin = min(colA, colB);
            fixed4 colmax = max(colA, colB);

            //fixed4 collerp = lerp(colA, colB, clamp(abs(sin(_CPUTime * 0.2)), 0.4, 0.6));
            fixed4 collerp2 = lerp(colmin, colmax, noise(texcoord));


            return collerp2;
        }

        //CloudAnimate
        fixed4 tex2D_doubleAnimated_Cloud(sampler2D tex, float2 texcoord, float2 speedA, float2 speedB)
        {

            //fixed2 texcoordA = texcoord  + speedA/1000 * _CPUTime;
            fixed2 texcoordA = texcoord + speedA / 1000 * _Time.y;
            //fixed2 texcoordB = texcoord  + speedB/1000 * _CPUTime;
            fixed2 texcoordB = texcoord + speedB / 1000 * _Time.y;
            fixed4 colA = tex2D(tex, texcoordA);
            fixed4 colB = tex2D(tex, texcoordB);
            colB = fixed4(colB.r, colB.g, colB.b, colB.r);
            //fixed4 colmin = min(colA, colB);
            fixed4 colmax = max(colA, colB);

            //fixed4 collerp = lerp(colA, colB, clamp(abs(sin(_CPUTime * 0.2)), 0.4, 0.6));
            fixed4 collerp2 = colmax;


            return collerp2;
        }


        //UVRotation
        float2 GetRotatedUV(float degree, float2 UV, float2 referenceUVScale) {
        
            float radian = pi * degree / 180;

            //_UVRotationDegree
            float sinX = sin(radian);
            float cosX = cos(radian);
            float sinY = sin(radian);
            float2x2 rotationMatrix = float2x2(cosX, -sinX, sinY, cosX);

            float2 rotatedUV = mul(UV - float2(0.5, 0.5) * referenceUVScale, rotationMatrix) + float2(0.5, 0.5)  ;
        
            return rotatedUV;
        }


        //spherical blend factor
        float getBlendFactorSpherical_local(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);

            float posy = (pos.y);
            float vy = (uv.y - posy);

            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            radius /= width;

            return  saturate((sphericalArea / radius - width));
        }

        //spherical blend factor for vertex
        float getBlendFactorSpherical_local_vert(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);



            float posy = (pos.y);
            float vy = (uv.y - posy);


            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            return  saturate(sphericalArea / radius);

        }

        //Vert modification 
        void vert(inout appdata_full v)
        {
            //(r = AO, g = height, b = rough, a = additional)

            fixed blendFactor = tex2Dlod(_MaskTex, v.texcoord).r;
            blendFactor = saturate(blendFactor + _MaskOffset);
            
            //fixed blendFactorAllToD = tex2Dlod(_MaskTex, v.texcoord).r;
            //blendFactorAllToD = saturate(blendFactor + _MaskOffset2);

            // VFX mask
            //fixed4 texcoord_VFXmask = fixed4(v.texcoord.xy * _VFXMask_ST.xy + _VFXMask_ST.zw, 1.0, 1.0);
            //fixed DirtMasKsample = tex2Dlod(_VFXMask, texcoord_VFXmask).b;
            

            //get the displacement factor and transform based on properties
            fixed4 overalltexcoord = fixed4(v.texcoord.xy, 1.0, 1.0);
            fixed4 texcoordA = fixed4(v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw, 1.0, 1.0);
            //fixed4 texcoordB = fixed4(v.texcoord.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw, 1.0, 1.0);
            //fixed4 texcoordD = fixed4(v.texcoord.xy * _AlbedoD_ST.xy , 1.0, 1.0);
            //fixed4 RotatedTexcoordD = fixed4(GetRotatedUV(_UVRotationDegree,texcoordD.xy, _AlbedoD_ST.xy)+_AlbedoD_ST.zw,1.0,1.0);

            //g value in the pack is displacement
            //fixed healthydisplacement = tex2Dlod_simpleblend(_PBRMapA, _PBRMapD, texcoordA, RotatedTexcoordD, DirtMasKsample).g;
            //fixed healthyScale = lerp(_DisplacementScaleA, _DisplacementScaleD, DirtMasKsample);
            //fixed healthyOffset = lerp(_DisplacementOffsetA, _DisplacementOffsetD, DirtMasKsample);

            //healthy to rewilded displacement



            fixed displacementAmount = lerp(tex2Dlod(_PBRMapA, texcoordA).g, saturate(tex2Dlod(_PBRMapB, texcoordA).g + _RiverDisplacementOffset), blendFactor);
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);

            //fixed Temp_displacement = displacementAmount;
            //displacementAmount = lerp(Temp_displacement, healthydisplacement, blendFactorAllToD);
            //displacementAmount *= lerp(1, healthyScale, blendFactorAllToD);
            //displacementAmount += lerp(0, _DisplacementOffsetA, blendFactorAllToD);

            //a value in the pack is Addional displacement in A set
            fixed overalldisplacement = tex2Dlod(_PBRMapA, overalltexcoord).a;

            //r value in water mask as pond displacement
            fixed4 VFXmasktexcoord = fixed4((v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw), 1.0, 1.0);
            fixed4 VFXsample = tex2Dlod(_VFXMask, VFXmasktexcoord);
            fixed RewildRiverdisplacementmask = VFXsample.r;

            fixed dynamicdisplace = 0;
            fixed OutPutDisplacement = displacementAmount;

            //if (_EnableDistanceFade > 0) {
                //fixed3 worldpos = mul(unity_ObjectToWorld, v.vertex);
                //fixed CycleFactor = getBlendFactorSpherical_local_vert(_FadePosition3D.xz, _FadeWidth, _FadeRadius * 5 , worldpos.xz, _FadeScaleFactor);
                //dynamicdisplace = lerp(_SubDisplace, 0.0, CycleFactor);
                //OutPutDisplacement = lerp(displacementAmount, 0.0, CycleFactor*(1 - _MaskOffset2)) + lerp(dynamicdisplace,0, saturate(_MaskOffset2));
            //}

            
            v.vertex.y += OutPutDisplacement;
            v.vertex.y += (overalldisplacement * _OverallDisplacement);
            v.vertex.y = (v.vertex.y * 1- RewildRiverdisplacementmask) + (RewildRiverdisplacementmask * _WaterDisplacement);
            v.color.xy = texcoordA.xy;
            //v.color.zw = texcoordB.xy;
            //v.texcoord1.xy = RotatedTexcoordD.xy;
            
        }

        //main blend surface shading
        void surf_main(Input IN, inout SurfaceOutputStandard o, float blendFactor)
        {
            //(r = AO, g = height, b = rough, a = additional)
            fixed2 texcoordA = IN.color.xy;
            //fixed2 texcoordB = IN.color.zw;
            //fixed2 texcoordD = fixed2(IN.uv_MaskTex.xy * _AlbedoD_ST.xy);
            //texcoordD = GetRotatedUV(_UVRotationDegree, texcoordD) + _AlbedoD_ST.zw;
            fixed4 Asample = tex2D(_PBRMapA, texcoordA);
            fixed4 Bsample = tex2D(_PBRMapB, texcoordA);
            //fixed4 Dsample = tex2D(_PBRMapD, texcoordD);
            

            fixed2 texcoord_RewildedWaterNormal = fixed2(IN.uv_MaskTex.xy * _RewildedWaterNormal_ST.xy + _RewildedWaterNormal_ST.zw);
            //fixed2 texcoord_DirtTexture = fixed2(IN.uv_MaskTex.xy *_DirtTexture_ST.xy + _DirtTexture_ST.zw);
            //fixed4 DirtTextureSample = tex2D(_DirtTexture, texcoord_DirtTexture);

            fixed2 texcoord_VFXmask = fixed2(IN.uv_MaskTex.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw);
            fixed2 texcoord_Cloud = fixed2(IN.uv_MaskTex.xy * _VFXMask_ST.xy + _VFXMask_ST.zw);
            fixed4 VFXsample = tex2D(_VFXMask, texcoord_VFXmask);
            fixed RewildedWaterMaskSample = VFXsample.r;
            fixed DewildedWaterMaskSample = VFXsample.b;
            //fixed HealthyDirtMaskSample = VFXsample.b;
            //fixed CloudMaskSample = VFXsample.g;
            fixed4 WaterNormalAnimateSample = tex2D_doubleAnimated(_RewildedWaterNormal, texcoord_RewildedWaterNormal, _OceanSpeedA, _OceanSpeedB);
            fixed4 RewildedWaterNormalSample = (WaterNormalAnimateSample) *RewildedWaterMaskSample;
            fixed4 DewildedWaterNormalSample = (WaterNormalAnimateSample)*DewildedWaterMaskSample;
            fixed CloudMaskAnimateSample = saturate(tex2D_doubleAnimated_Cloud(_VFXMask, texcoord_Cloud, _CloudSpeed.xy, _CloudSpeed.zw).g - _CloudOffset);


            
        
            /*#ifdef SLOTA_IS_EMPTY
            if (blendFactor < 0.5) discard;
            #endif

            #ifdef SLOTB_IS_EMPTY
            if (blendFactor > 0.5) discard;
            #endif*/

            

            //Albedo;
            fixed3 color;

            fixed3 color_healthy;
            //fixed4 color_healthy2;
            fixed3 color_unhealthy;

            //fixed4 color2;

            color_healthy = Saturation(tex2D(_AlbedoA, texcoordA).rgb,_SaturationA) * _TintA.rgb;
            color_unhealthy = Saturation(tex2D(_AlbedoB, texcoordA).rgb,_SaturationB) * _TintB.rgb;
            //color_healthy2 = tex2D(_AlbedoD, texcoordD);

            //Healthy1 X Healthy2
            //fixed4 color_healthy_mod = lerp(color_healthy, color_healthy2, HealthyDirtMaskSample);
            fixed watermod_Rewild = saturate(RewildedWaterMaskSample - _WaterColor.a);
            fixed watermod_Dewild = saturate(DewildedWaterMaskSample - _WaterColor2.a);
            fixed3 healthyColor_water = color_healthy.rgb * (1 - watermod_Rewild) + _WaterColor.rgb * watermod_Rewild;
            fixed3 unhealthyColor_water = color_unhealthy.rgb * (1 - watermod_Dewild) + _WaterColor2.rgb * watermod_Dewild;
            //fixed4 color_water = lerp(color_healthy, healthyColor_water,_NoCircleOffset);

            color = lerp(healthyColor_water, unhealthyColor_water, blendFactor);

            
            //color2 = lerp(color, healthy2Color_water, blendFactor2);

            fixed3 cloudcolor = fixed3(0, 0, 0);
            fixed3 finalcolor = color.rgb * (1 - CloudMaskAnimateSample) + cloudcolor * CloudMaskAnimateSample;
            o.Albedo = finalcolor;

            //Normal

            fixed4 healthyNormal;

            fixed4 Avalue = tex2D(_NormalA, texcoordA);
            fixed4 Bvalue = tex2D(_NormalB, texcoordA);
            //fixed4 D_normal = tex2D(_NormalD, texcoordD);

            //Healthy1 X Healthy2
            //healthyNormal = lerp(Avalue, D_normal, HealthyDirtMaskSample);
            fixed4 healthy_normal = Avalue * (1 - RewildedWaterMaskSample) + RewildedWaterNormalSample;
            fixed4 unhealthy_normal = Bvalue * (1 - DewildedWaterMaskSample) + DewildedWaterNormalSample;
            //fixed4 rewilded_normal = lerp(Avalue, end_normal,_NoCircleOffset);
            fixed4 temp_normal = lerp(healthy_normal, unhealthy_normal, blendFactor);
            
            o.Normal = UnpackNormal(temp_normal);

            //Occlusion = r

            fixed healthy_ao;
            fixed healthy_ao_mod;

            //Healthy1 X Healthy2

            //healthy_ao = lerp(Asample.r, Dsample.r, HealthyDirtMaskSample);
            healthy_ao_mod = lerp(1, Asample.r, _OcclusionSlider);
            //fixed rewildedHealthy_ao = healthy_ao_mod;

            fixed temp_Occlusion = lerp(healthy_ao_mod, Bsample.r * _AoB, blendFactor);

            
            //o.Occlusion = lerp(temp_Occlusion, healthy_ao_mod, blendFactor2);
            o.Occlusion = saturate(temp_Occlusion - _OverallAO);

            //metallic = a
            //fixed4 metalsmooth = tex2D_2LayerBlend(_PBRMapA, _PBRMapB, texcoordA, texcoordB, blendFactor).a;
            //fixed4 metalsmooth_3 = tex2D_2LayerBlend(metalsmooth, _PBRMapD, texcoordD, blendFactor2).a;


            //Smoothness = b 

            //fixed healthy_roughness = lerp(Asample.b, Dsample.b, HealthyDirtMaskSample);
           // fixed end_roughness = RewildedWaterMaskSample;
            //fixed roughnessfilter = lerp(Asample.b * _SmoothnessA, _SmoothnessD * (RewildedWaterMaskSample + Asample.b), _NoCircleOffset) ;
            fixed roughnessfilter = Asample.b * _SmoothnessA;
            //fixed temp_smoothvalue = lerp(_SmoothnessA, _SmoothnessB, blendFactor);
            fixed temp_roughness = lerp(roughnessfilter, Bsample.b * _SmoothnessB, blendFactor);
            //fixed end_roughness = healthy_roughness * (RewildedWaterMaskSample) + (1-RewildedWaterMaskSample);

           // o.Smoothness = lerp(temp_smoothvalue, _SmoothnessD, blendFactor2) *  (roughnessfilter);
            o.Smoothness = temp_roughness;
            o.Metallic = 0;


            //Alpha in Albedo
            o.Alpha = 1;
            //o.Alpha *= lerp(1.0, saturate(1 - CloseOceanMaskSample + _WaterTransparency), blendFactor2);

            //blend edge

            
            //_EdgeValue += EdgeNoiseSample;

            if (blendFactor < _EdgeValue) {
                blendFactor = 1.0 - saturate((_EdgeValue - blendFactor) / _EdgeWidth);
            }
            else {
                blendFactor = 1.0 - saturate((blendFactor - _EdgeValue) / _EdgeWidth);
            }

            ////blend edge 2
            //if (blendFactor2 < _EdgeValue) {
            //    blendFactor2 = 1.0 - saturate((_EdgeValue - blendFactor2) / _EdgeWidth);
            //}
            //else {
            //    blendFactor2 = 1.0 - saturate((blendFactor2 - _EdgeValue) / _EdgeWidth);
            //}

            fixed3 edgeCol = _EdgeColorA;
            //o.Albedo = lerp(lerp(o.Albedo, edgeCol, blendFactor), edgeCol, blendFactor2);
            o.Albedo = lerp(o.Albedo, edgeCol, blendFactor);
            //o.Albedo *= o.Occlusion;
        }



        //end shading
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed MaskSample = tex2D(_MaskTex, IN.uv_MaskTex).r;
            fixed EdgeNoiseSample = tex2D(_EdgeNoise, IN.uv_MaskTex * _EdgeNoise_ST.xy + _EdgeNoise_ST.zw).r;
            fixed blendFactor = saturate(clamp(MaskSample,0.3,1) + (clamp(MaskSample,0,0.3) * EdgeNoiseSample - 0.3) + _MaskOffset);
            
            //fixed blendFactor = IN.factors.x;

            //fixed blendFactorAllToD = saturate(MaskSample + _MaskOffset2);

            //fixed blendFactorAllToD = IN.factors.y;

            

            surf_main(IN, o, blendFactor);
            


            fixed revealmasksample = tex2D(_RevealMask, IN.uv_RevealMask).r;
            fixed alphamodstep = lerp(0.0, _AlphaMod, revealmasksample);

            fixed revealAlpha = saturate((revealmasksample + alphamodstep) * _GeneralAlpha);

            fixed EdgeFadeAlpha = 1;

            if (_EnableDistanceFade > 0) {
                fixed CycleFactor = getBlendFactorSpherical_local(_FadePosition3D.xz, _FadeWidth, _FadeRadius, IN.worldPos.xz, _FadeScaleFactor);
                EdgeFadeAlpha = lerp(1.0, 0.0, CycleFactor);
            }
            o.Metallic = _DarkenSlider;
            o.Alpha *= EdgeFadeAlpha * revealAlpha;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
