﻿Shader "HeightBlend/SurfaceBlendWipe"
{
    Properties
    {
        _AlphaMod("Alpha modification",Range(-1.0,1.0)) = 0.0
        _GeneralAlpha("General Transparency", Range(0.0,2.0)) = 1.5


        //DistanceFade
        _FadeRadius("FadeRadius",Range(0.0,10.0)) = 0.0
        _FadePosition3D("Fade Center Position in 3D space",Vector) = (0,0,0,0)
        _FadeScaleFactor("Fade Scale Factor (XY)",Vector) = (1,1,0,0)
        _FadeWidth("FadeWidth",Range(-2.0,2.0)) = 0.1
        

        //wipe 
        _WipePosition("Wipe Position", Range(-1.0, 2.0)) = 0.5
        _WipeWidth("Wipe Width", Range(0.0, 1.0)) = 0.1
        [Enum(Horizontal, 0, Vertical, 1)] _WipeDirection("Wipe Direction", int) = 0
        _WipeOffset("Wipe Offset", float) = 0
        _DisplacementPinchDistance("Displacement Pinch Distance", Range(0.0, 1.0)) = 0.0
        _DisplacementPinchOffset("Displacement Pinch Offset", Range(0.0, 1.0)) = 0.0


        _Mode("blend mode",float) = 0.0

        [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend("__src", int) = 0
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend("__dst", int) = 1
        _ZWrite("__zw", int) = 0

		//[Toggle(Is_Ocean)] _IsOcean("Is it ocean?", float) = 1


        _AlbedoA ("Albedo A", 2D) = "white" {}
        _MetallicA ("MetallicA", 2D) = "black" {}
        [Normal] _NormalA ("NormalA", 2D) = "bump" {}
        _OcclusionA ("Occlusion A", 2D) = "white" {}
        _DisplacementA ("Displacement A", 2D) = "grey" {}
        _EmissionA ("Emission A", 2D) = "black" {}
        
        _DisplacementScaleA ("Displacement Scale A", Range(0.0, 10)) = 0.5
        _DisplacementOffsetA ("Displacement Offset A", Range(-1.0, 1.0)) = 0.0
        
        _AlbedoB ("Albedo B", 2D) = "white" {}
        _MetallicB ("Metallic B", 2D) = "black" {}
        [Normal] _NormalB ("Normal B", 2D) = "bump" {}
        _OcclusionB ("Occlusion B", 2D) = "white" {}
        _DisplacementB ("Displacement B", 2D) = "grey" {}
        _EmissionB ("Emission B", 2D) = "black" {}
        
        _DisplacementScaleB ("Displacement Scale B", Range(0.0, 10)) = 0.5
        _DisplacementOffsetB ("Displacement Offset B", Range(-1.0, 1.0)) = 0.0

		
        _RevealMask("Revealing mask",2D) = "white" {}

    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent-2" }
        LOD 200

        
        Blend [_SrcBlend] [_DstBlend]
        

        ZWrite [_ZWrite]

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert keepalpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.0
        
        #pragma shader_feature SLOTA_IS_EMPTY
        #pragma shader_feature SLOTB_IS_EMPTY
        #pragma shader_feature A_Is_Ocean
        #pragma shader_feature B_Is_Ocean
        #pragma shader_feature _HAILALPHABLEND

        sampler2D _MainTex;

        struct Input
        {
            float3 localPos;
            float3 worldPos;
            float2 texcoord;
            float2 texcoordA;
            float2 texcoordB;
            float2 uv_RevealMask;
        };

        //mask stuff
        float _WipePosition;
        float _WipeWidth;
        int _WipeDirection;
        float _WipeOffset;
        
        float _DisplacementPinchDistance;
        float _DisplacementPinchOffset;
        
        //Reveal mask
        sampler2D _RevealMask;
        half _GeneralAlpha;
        half _AlphaMod;

        //EdgeFade
        float _FadeRadius;
        float4 _FadePosition3D;
        float4 _FadeScaleFactor;
        float _FadeWidth;

        
        //texture A stuff
        sampler2D _AlbedoA;
        float4 _AlbedoA_ST;
        sampler2D _MetallicA;
        sampler2D _NormalA;
        sampler2D _OcclusionA;
        sampler2D _DisplacementA;
        sampler2D _EmissionA;
        
        float _DisplacementScaleA;
        float _DisplacementOffsetA;
        
        //texture B stuff
        sampler2D _AlbedoB;
        float4 _AlbedoB_ST;
        sampler2D _MetallicB;
        sampler2D _NormalB;
        sampler2D _OcclusionB;
        sampler2D _DisplacementB;
        sampler2D _EmissionB;
        
        float _DisplacementScaleB;
        float _DisplacementOffsetB;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)
        
        //just to make the code easier to read/write
        //blends between two texture samples
        float4 tex2D_blend(sampler2D a, sampler2D b, float2 texcoordA, float2 texcoordB, float blendFactor)
        {
            #ifdef B_Is_Ocean
			float2 oceanuv1 = texcoordB + float2(0.01123, -0.0085) * _Time.y;
			float2 oceanuv2 = texcoordB + float2(-0.00961, 0.0155321) * _Time.y;
			float4 ocean1 = tex2D(b, oceanuv1);
			float4 ocean2 = tex2D(b, oceanuv2);
			float4 ocean = min(ocean1, ocean2);

			return lerp(tex2D(a, texcoordA), ocean, blendFactor);

            #elif A_Is_Ocean
			float2 oceanuv1 = texcoordA + float2(0.01123, -0.0085) * _Time.y;
			float2 oceanuv2 = texcoordA + float2(-0.00961, 0.0155321) * _Time.y;
			float4 ocean1 = tex2D(a, oceanuv1);
			float4 ocean2 = tex2D(a, oceanuv2);
			float4 ocean = min(ocean1, ocean2);

			return lerp(ocean, tex2D(b, texcoordB), blendFactor);

            #else
			
			return lerp(tex2D(a, texcoordA), tex2D(b, texcoordB), blendFactor);
            #endif
        }
        
        //just to make the code easier to read/write
        //blends between two texture samples (using tex2Dlod i.e. from the vertex shader)
        float4 tex2Dlod_blend(sampler2D a, sampler2D b, float4 texcoordA, float4 texcoordB, float blendFactor)
        {

   //         #ifdef B_Is_Ocean
			//float2 oceanuv1 = texcoordB + float2(0.01123, -0.0085) * _Time.y;
			//float2 oceanuv2 = texcoordB + float2(-0.00961, 0.0155321) * _Time.y;
			//float4 ocean1 = tex2Dlod(b, oceanuv1);
			//float4 ocean2 = tex2Dlod(b, oceanuv2);
			//float4 ocean = min(ocean1, ocean2);

			//return lerp(tex2Dlod(a, texcoordA), ocean, blendFactor);

   //         #elif A_Is_Ocean
			//float2 oceanuv1 = texcoordA + float2(0.01123, -0.0085) * _Time.y;
			//float2 oceanuv2 = texcoordA + float2(-0.00961, 0.0155321) * _Time.y;
			//float4 ocean1 = tex2Dlod(a, oceanuv1);
			//float4 ocean2 = tex2Dlod(a, oceanuv2);
			//float4 ocean = min(ocean1, ocean2);

			//return lerp(ocean, tex2Dlod(b, texcoordB), blendFactor);

   //         #else


            return lerp(tex2Dlod(a, texcoordA), tex2Dlod(b, texcoordB), blendFactor);

           // #endif
        }
        
        float getDisplacementPinch(float distance, float2 texcoord)
        {
            if(_DisplacementPinchDistance <= 0) return 1.0;
            
            float top = saturate((1.0 - (texcoord.y + _DisplacementPinchOffset)) / _DisplacementPinchDistance);
            float bottom = saturate((texcoord.y - _DisplacementPinchOffset) / _DisplacementPinchDistance);
            return top * bottom;
        }

        float getBlendFactorSpherical_3D(float3 pos, float width, float radius, float3 pixelworldpos, float3 scalefactor) {

            float vx = (pixelworldpos.x - pos.x);
            float vy = (pixelworldpos.y - pos.y);
            float vz = (pixelworldpos.z - pos.z);

            float3 v = float3 (vx, vy, vz);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2) + pow(v.z / scalefactor.z, 2)));

            return saturate(sphericalArea / radius + width);
        }


        float getBlendFactorSpherical_local(float2 pos, float width, float radius, float2 uv, float2 scalefactor) {

            float posx = (pos.x);
            float vx = (uv.x - posx);



            float posy = (pos.y);
            float vy = (uv.y - posy);


            float2 v = float2 (vx, vy);

            float sphericalArea = abs(sqrt(pow(v.x / scalefactor.x, 2) + pow(v.y / scalefactor.y, 2)));

            return  saturate(sphericalArea / radius + width);

        }
        
        float getBlendFactor(float pos, float size, int direction, float offset, float2 uv)
        {
            if(direction <= 0) {
                float relativePos = ((uv.x + offset) % 1.0) - pos;
                relativePos += size;
                return 1.0 - saturate(relativePos / (size * 2.0));
            } else {
                float relativePos = ((uv.y + offset) % 1.0) - pos;
                relativePos += size;
                return 1.0 - saturate(relativePos / (size * 2.0));
            }
        }
        
        void vert (inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            
            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
            float blendFactor = getBlendFactor(_WipePosition, _WipeWidth, _WipeDirection, _WipeOffset, v.texcoord.xy);
            
            //get the displacement factor and transform based on properties
            float4 texcoordA = float4(v.texcoord.xy * _AlbedoA_ST.xy + _AlbedoA_ST.zw, 1.0, 1.0);
            float4 texcoordB = float4(v.texcoord.xy * _AlbedoB_ST.xy + _AlbedoB_ST.zw, 1.0, 1.0);
            float displacementAmount = tex2Dlod_blend(_DisplacementA, _DisplacementB, texcoordA, texcoordB, blendFactor);
            displacementAmount *= lerp(_DisplacementScaleA, _DisplacementScaleB, blendFactor);
            displacementAmount += lerp(_DisplacementOffsetA, _DisplacementOffsetB, blendFactor);
            displacementAmount *= getDisplacementPinch(_DisplacementPinchDistance, v.texcoord.xy);
            
            //actually offset the vertices - note that y = up
            v.vertex += float4(v.normal * displacementAmount, 0.0);
            
            o.localPos = v.vertex.xyz;

            o.texcoordA = texcoordA.xy;
            o.texcoordB = texcoordB.xy;
            o.texcoord = v.texcoord;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            //get the blend factor from the mask texture
            //note - we could totally use the other three channels to add more blend targets!
            float blendFactor = getBlendFactor(_WipePosition, _WipeWidth, _WipeDirection, _WipeOffset, IN.texcoord.xy);
            
            #ifdef SLOTA_IS_EMPTY
            if(blendFactor < 0.5) discard;
            #endif

            #ifdef SLOTB_IS_EMPTY
			if (blendFactor > 0.5) discard;
            #endif
            
             //lerp between all the maps and set them to the appropriate output channels
            //o.Albedo = lerp(tex2D(_AlbedoA, IN.texcoordA), tex2D(_AlbedoB, IN.texcoordB), blendFactor);
			o.Albedo = tex2D_blend(_AlbedoA,_AlbedoB, IN.texcoordA, IN.texcoordB, blendFactor);
            float4 metalsmooth = tex2D_blend(_MetallicA, _MetallicB, IN.texcoordA, IN.texcoordB, blendFactor);
            o.Metallic = 0;

            #ifdef B_Is_Ocean

			o.Smoothness = lerp(metalsmooth.a, 0.8, blendFactor);

            #elif A_Is_Ocean

			o.Smoothness = lerp(0.8, metalsmooth.a, blendFactor);

            #else

			o.Smoothness = metalsmooth.a;
			
            #endif
            
            o.Normal = UnpackNormal(tex2D_blend(_NormalA, _NormalB, IN.texcoordA, IN.texcoordB, blendFactor));
            o.Occlusion = tex2D_blend(_OcclusionA, _OcclusionB, IN.texcoordA, IN.texcoordB, blendFactor).r;
            o.Emission = tex2D_blend(_EmissionA, _EmissionB, IN.texcoordA, IN.texcoordB, blendFactor).rgb;
            o.Albedo *= o.Occlusion;



            #ifdef _HAILALPHABLEND
            
            float revealmasksample = tex2D(_RevealMask, IN.uv_RevealMask).r;
            half alphamodstep = lerp(0.0, _AlphaMod, revealmasksample);

            float revealAlpha = saturate((revealmasksample + alphamodstep) * _GeneralAlpha);

            //

            //float CycleFactor = getBlendFactorSpherical_3D(_FadePosition3D.xyz, _FadeWidth, _FadeRadius, IN.localPos.xyz, _FadeScaleFactor);
            float CycleFactor = getBlendFactorSpherical_local(_FadePosition3D.xz, _FadeWidth, _FadeRadius, IN.worldPos.xz, _FadeScaleFactor);


            float EdgeFadeAlpha = lerp(1.0, 0.0, CycleFactor);


            //


            o.Alpha = EdgeFadeAlpha * revealAlpha;

            #else

            o.Alpha = 1.0;

            #endif
        }
        ENDCG
    }


    
    //use the custom editor
    CustomEditor "SurfaceBlendWipeGUI"
    FallBack "Diffuse"
}
