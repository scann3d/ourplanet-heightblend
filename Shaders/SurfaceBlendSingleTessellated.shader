﻿Shader "HeightBlend/SurfaceSingleTessellated"
{
    Properties
    {
        _Albedo ("Albedo", 2D) = "white" {}
        _Metallic ("Metallic", 2D) = "black" {}
        [Normal] _Normal ("Normal", 2D) = "bump" {}
        _Occlusion ("Occlusion", 2D) = "white" {}
        _Displacement ("Displacement", 2D) = "grey" {}
        _Emission ("Emission", 2D) = "black" {}
        _DisplacementScale ("Displacement Scale", Range(0.0, 10)) = 0.5
        _DisplacementOffset ("Displacement Offset", Range(-1.0, 1.0)) = 0.0
        _Tess ("Tesselation Factor", Range(1, 32)) = 4
        _MinDist ("Minimum Distance", Range(0, 20)) = 1
        _MaxDist ("Maximum Distance", Range(0, 20)) = 5
        _TextureScale ("Texture Scale", Vector) = (1, 1, 0, 0)
        _DisplacementPinchDistance("Displacement Pinch Distance", Range(0.0, 1.0)) = 0.0
        _DisplacementPinchOffset("Displacement Pinch Offset", Range(0.0, 1.0)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert tessellate:tessDistance

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 4.6
        #include "Tessellation.cginc"

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_Albedo;
        };
        
        struct appdata_custom
        {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 tangent : TANGENT;
            float2 texcoord : TEXCOORD0;
            //float2 texcoord_scale : TEXCOORD1;
        };
        
        sampler2D _Albedo;
        float2 _TextureScale;
        sampler2D _Metallic;
        sampler2D _Normal;
        sampler2D _Occlusion;
        sampler2D _Displacement;
        sampler2D _Emission;
        
        int _Tess;
        float _MinDist;
        float _MaxDist;
        
        float _DisplacementScale;
        float _DisplacementOffset;
        
        float _DisplacementPinchDistance;
        float _DisplacementPinchOffset;
        
        float4 tessDistance (appdata_custom v0, appdata_custom v1, appdata_custom v2) {
            return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _MinDist, _MaxDist, _Tess);
        }
        
        float getDisplacementPinch(float distance, float2 texcoord)
        {
            if(_DisplacementPinchDistance <= 0) return 1.0;
            
            float top = saturate((1.0 - (texcoord.y + _DisplacementPinchOffset)) / _DisplacementPinchDistance);
            float bottom = saturate((texcoord.y - _DisplacementPinchOffset) / _DisplacementPinchDistance);
            return top * bottom;
        }
        
        void vert (inout appdata_custom v)
        {                        
            //get the displacement factor and transform based on properties
            float displacementAmount = tex2Dlod(_Displacement, float4(v.texcoord.xy * _TextureScale, 1.0, 1.0));
            displacementAmount *= _DisplacementScale;
            displacementAmount += _DisplacementOffset;
            displacementAmount *= getDisplacementPinch(_DisplacementPinchDistance, v.texcoord.xy);
            
            //actually offset the vertices - note that y = up
            v.vertex += float4(v.normal * displacementAmount, 0);
            
            //v.texcoord_scale = v.texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float4 col = tex2D(_Albedo, IN.uv_Albedo * _TextureScale);
            float4 metalSmooth = tex2D(_Metallic, IN.uv_Albedo * _TextureScale);
            
            //lerp between all the maps and set them to the appropriate output channels
            o.Albedo = col.rgb;
            o.Metallic = metalSmooth.r;
            o.Smoothness = metalSmooth.a;
            o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_Albedo * _TextureScale));
            o.Occlusion = tex2D(_Occlusion, IN.uv_Albedo * _TextureScale);
            o.Emission = tex2D(_Emission, IN.uv_Albedo * _TextureScale);
        }
        ENDCG
    }
    
    FallBack "Diffuse"
}
