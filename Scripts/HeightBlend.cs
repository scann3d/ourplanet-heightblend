﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PHORIA.HeightBlend
{

	/// <summary>
	/// Class for painting onto HeightMaps for displacement materials
	/// </summary>
	public class HeightBlend : MonoBehaviour
	{

		private static HeightBlend _instance;
		/// <summary>
		/// Gets the Singleton instance of HeightBlend
		/// </summary>
		public static HeightBlend Instance
		{
			get
			{
				if (_instance == null) _instance = GameObject.FindObjectOfType<HeightBlend>();
				return _instance;
			}
		}

		
		public delegate void PaintEvent(object sender, PaintEventArgs e);
		public delegate void ResetEvent(object sender, EventArgs e);

		public class PaintEventArgs : EventArgs
		{
			public Vector3 WorldPosition;
			public Texture2D Brush;
			public float Radius;
			public float TargetValue;
			public float Opacity;
		}

		public event PaintEvent OnPaint;
		public event ResetEvent OnReset;

		/// <summary>
		/// Size of the RenderTexture mask used for each HeightBlendTarget. Must be a power of two.
		/// Not changeable at runtime
		/// </summary>
		[Tooltip("Size of the RenderTexture mask used for each HeightBlendTarget. Must be a power of two. Not changeable at runtime!")]
		public int MaskSize = 1024;

		/// <summary>
		/// Whether the HeightBlend system is active. Turn this off when not in use to save performance
		/// </summary>
		public bool Active = true;

		/// <summary>
		/// If true, the height distance of brushes from HeightBlendTargets is taken into consideration
		/// </summary>
		[Tooltip("If true, the height distance of brushes from HeightBlendTargets is taken into consideration")]
		public bool UseSphericalAttenuation = true;

		private List<HeightBlendTarget> _targets;

		/// <summary>
		/// Sets the HeightBlend Active status to value
		/// </summary>
		/// <param name="value">The desired new value for HeightBlend.Active</param>
		public void SetActive(bool value)
		{
			Active = value;
		}

		/// <summary>
		/// 	Multiplying the radius in Paint() by a globalRadius allows us to shrink or grow at a constant
		/// 	rate in the event that the content itself has been resized.
		/// </summary>
		public float globalRadius = 1.0f;
		
		/// <summary>
		/// Attempts to paint on all HeightBlendTargets at the given world position
		/// </summary>
		/// <param name="worldPos">Center world position of brush</param>
		/// <param name="brush">Brush texture to paint onto masks</param>
		/// <param name="radius">Radius of brush, in meters</param>
		/// <param name="targetValue">Target mask value to paint, from 0 - 1</param>
		/// <param name="opacity">Opacity of the brush, from 0 - 1</param>
		public void Paint(Vector3 worldPos, Texture2D brush, float radius, float targetValue, float opacity = 1f)
		{
			if(_targets == null) _targets = new List<HeightBlendTarget>();
			if (!Active) return;

            //radius *= globalRadius;

            Debug.Log($"About to paint with radius : {radius}");

			foreach (var t in _targets) {
				t.Paint(worldPos, brush, radius, targetValue, opacity);
			}

			//sends OnPaint event for HeightBlendAnim if it exists
			OnPaint?.Invoke(this, new PaintEventArgs()
			{
				WorldPosition = worldPos, 
				Brush = brush, 
				Radius = radius, 
				TargetValue = targetValue, 
				Opacity = opacity
			});
		}
		
		/// <summary>
		/// Attempts to paint on all HeightBlendTargets at the given world position
		/// </summary>
		/// <param name="worldPos">Center world position of brush</param>
		/// <param name="worldRotation">Brush rotation</param>
		/// <param name="brush">Brush texture to paint onto masks</param>
		/// <param name="radius">Radius of brush, in meters</param>
		/// <param name="targetValue">Target mask value to paint, from 0 - 1</param>
		/// <param name="opacity">Opacity of the brush, from 0 - 1</param>
		public void Paint(Vector3 worldPos, Quaternion worldRotation, Texture2D brush, float radius, float targetValue, float opacity = 1f)
		{
			if(_targets == null) _targets = new List<HeightBlendTarget>();
			if (!Active) return;

            //radius *= globalRadius;
            Debug.Log($"About to paint with radius : {radius}");

            foreach (var t in _targets) {
				t.Paint(worldPos, worldRotation, brush, radius, targetValue, opacity);
			}

			//sends OnPaint event for HeightBlendAnim if it exists
			OnPaint?.Invoke(this, new PaintEventArgs()
			{
				WorldPosition = worldPos, 
				Brush = brush, 
				Radius = radius, 
				TargetValue = targetValue, 
				Opacity = opacity
			});
		}

		/// <summary>
		/// Creates and returns a new RenderTexture to be used as a mask
		/// </summary>
		public RenderTexture GetRenderTexture()
		{
			var template = Resources.Load<RenderTexture>("TemplateRenderTexture");
			var outputRenderTexture = new RenderTexture(MaskSize, MaskSize, 32, template.format);
			outputRenderTexture.Create();
			outputRenderTexture.MarkRestoreExpected();
			//template.DiscardContents();
			return outputRenderTexture;
		}

		/// <summary>
		/// Wipes all render textures (resetting to black)
		/// </summary>
		public void ClearAll()
		{
			foreach (var t in _targets) {
				t.Clear();
			}
			
			//Sends OnReset event for HeightBlendAnim if it exists
			OnReset?.Invoke(this, new EventArgs());
		}
		
		/// <summary>
		/// Wipes all render textures (resetting to black or white)
		/// </summary>
		/// <param name="targetColor">Color to reset textures to (black or white only)</param>
		public void ClearAll(Color targetColor)
		{
			if (targetColor != Color.black && targetColor != Color.white)
				throw new System.FormatException("HeightBlend.CleaAll only accepts Color.black or Color.white");
			foreach (var t in _targets) {
				t.Clear(targetColor == Color.black);
			}
			
			//Sends OnReset event for HeightBlendAnim if it exists
			OnReset?.Invoke(this, new EventArgs());
		}

		/// <summary>
		/// Adds a new target to the target list to be painted onto
		/// </summary>
		/// <param name="target">The target to add</param>
		public void AddTarget(HeightBlendTarget target)
		{
			if (_targets == null) _targets = new List<HeightBlendTarget>();
			_targets.Add(target);
		}

		/// <summary>
		/// Removes a target from the target list (to skip painting it)
		/// </summary>
		/// <param name="target">The target to remove</param>
		public void RemoveTarget(HeightBlendTarget target)
		{
			_targets?.Remove(target);
		}
	}

}