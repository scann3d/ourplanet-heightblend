﻿using UnityEngine;

namespace PHORIA.HeightBlend.Examples
{

    public class HeightBlendTransformBrush : MonoBehaviour
    {

        public Texture2D BrushTexture;
        public Texture2D[] Sprites;
        [Range(1, 20)] public int SpriteInterval = 5;
        
        public float Radius;
        public bool SnapToGround;
        [Range(0f, 1f)] public float TargetValue = 1f;
        public float Opacity = 1f;

        private int _spriteIndex;

        public void Update()
        {
            
            
            
            
            var drawPos = transform.position;
            if (SnapToGround) drawPos.y = 0f;
            
            if (Sprites == null || Sprites.Length == 0) {
                HeightBlend.Instance.Paint(drawPos, transform.rotation, BrushTexture, Radius, TargetValue, Opacity);
                return;
            }
            
            HeightBlend.Instance.Paint(drawPos, transform.rotation, Sprites[_spriteIndex], Radius, TargetValue, Opacity);
            
            if (Time.frameCount % SpriteInterval != 0) return;
            _spriteIndex++;
            if (_spriteIndex >= Sprites.Length) _spriteIndex = 0;
        }
    }

}