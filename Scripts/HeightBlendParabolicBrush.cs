﻿using System.Diagnostics;
using PHORIA.HeightBlend.Examples;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace PHORIA.HeightBlend
{
	public class HeightBlendParabolicBrush : MonoBehaviour
	{

		public float EmitSpeed = 7.5f;
		public LayerMask TargetLayerMask;
		public float TimeStep = 0.2f;
		public float EndHeight = -1f;
		public float GravityStrength = -9.8f;
		public int MaxSteps = 50;
		public HeightBlendTransformBrush Positionee;
        public bool HitFlag = false;

		protected virtual void Update()
		{
			TimeStep = Mathf.Max(0.001f, TimeStep);
			
			var pos = transform.position;
			var vel = transform.forward * EmitSpeed;
			var debugLerp = 0f;
			var count = 0;
			var hasHit = false;
            
			RaycastHit hit;
			while (pos.y > EndHeight) {
				Debug.DrawLine(pos, pos + vel * TimeStep, Color.Lerp(Color.red, Color.blue, debugLerp % 1f));
				
				if (Physics.Raycast(pos, vel, out hit, vel.magnitude * TimeStep, TargetLayerMask)) {
					HandleHit(hit);
					hasHit = true;
					break;
				}

				pos += vel * TimeStep;
				vel += new Vector3(0f, GravityStrength, 0f) * TimeStep;
				debugLerp += 0.1f;
				count++;
				if (count > MaxSteps) {
					break;
				}
			}

			Positionee.gameObject.SetActive(hasHit);
            
        }

		private void HandleHit(RaycastHit hit)
		{
			//Debug.DrawLine(hit.point + new Vector3(0f, -0.5f, 0f), hit.point + new Vector3(0f, 0.5f, 0f), Color.green);
			Positionee.transform.position = hit.point;
		}
	}
}