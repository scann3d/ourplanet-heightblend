using UnityEngine;
using System.Collections;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace PHORIA.HeightBlend
{
	[RequireComponent(typeof(Renderer))]
	[RequireComponent(typeof(MeshFilter))]
	public class HeightBlendTarget : MonoBehaviour
	{

		public Vector3 MeshScale;
		public Vector3 MeshOffset;

		private Material _mainMaterial;
		private Material _blitMaterial;
		private RenderTexture _renderTexture;
		private bool _initialized;
		
		private static readonly int MaskTex = Shader.PropertyToID("_MaskTex");
		
		private static readonly int BrushTex = Shader.PropertyToID("_BrushTex");
		private static readonly int MainTexSize = Shader.PropertyToID("_MainTexSize");
		private static readonly int BrushTexSize = Shader.PropertyToID("_BrushTexSize");
		private static readonly int BrushPos = Shader.PropertyToID("_BrushPos");
		private static readonly int Radius = Shader.PropertyToID("_Radius");
		private static readonly int TargetValue = Shader.PropertyToID("_TargetValue");
		private static readonly int Opacity = Shader.PropertyToID("_Opacity");

		private MeshFilter _meshFilter;
		private static readonly int Rotation = Shader.PropertyToID("_Rotation");

        private void Awake()
        {
            _meshFilter = GetComponent<MeshFilter>();
        }

        public void Start()
		{
            

        }

		public void OnEnable()
		{
			HeightBlend.Instance.AddTarget(this);
            //Initialize();
        }

		public void OnDisable()
		{
			//check for null because when we turn off the app the HeightBlend object might be disabled first
			if(HeightBlend.Instance != null)
				HeightBlend.Instance.RemoveTarget(this);

            _initialized = false;
		}

		public bool Paint(Vector3 worldPos, Texture2D brush, float radius, float targetValue, float opacity = 1f)
		{
			return Paint(worldPos, Quaternion.identity, brush, radius, targetValue, opacity);
		}
		
		public bool Paint(Vector3 worldPos, Quaternion rotation, Texture2D brush, float radius, float targetValue, float opacity = 1f)
		{
			if (!_initialized) Initialize();

			if (MeshOffset != Vector3.zero) {
				worldPos += MeshOffset;
			}

			var localPos = transform.InverseTransformPoint(worldPos);
			
			var radiusScale = 1f;
			if (MeshScale != Vector3.zero) {
				var finalMeshScale = new Vector3(1f / MeshScale.x, 1f, 1f / MeshScale.z);
				localPos.Scale(finalMeshScale);
				radiusScale = 1f / MeshScale.x;
			} else {
				var size = _meshFilter.mesh.bounds.size;
				var maxSize = Mathf.Max(size.x, Mathf.Max(size.y, size.z));
				var finalMeshScale = new Vector3(1f / maxSize, 1f, 1f / maxSize);
				localPos.Scale(finalMeshScale);
				radiusScale = 1f / maxSize;
			}

			radius *= radiusScale;

			if (HeightBlend.Instance.UseSphericalAttenuation) {
				//do spherical attenuation and don't draw anything if we're too far away
				var worldDistY = localPos.y * transform.localScale.y;
				worldDistY /= radius;
				worldDistY = Mathf.Clamp01(Mathf.Abs(worldDistY));
				radius *= Mathf.Sqrt(1f - worldDistY * worldDistY);

				if (radius <= 0f) return false;
			}

			//transform radius from meters to pixels (relative to render texture)
			radius *= _renderTexture.width / transform.localScale.x;
			
			//turn world pos into local pos
			var uvPos = GetUvPosition(localPos);
			var texturePos = GetTexturePosition(uvPos);

			//if out of bounds, don't draw anything
			if (texturePos.x < -radius / 2 || texturePos.x > _renderTexture.width + radius / 2) return false;
			if (texturePos.y < -radius / 2 || texturePos.y > _renderTexture.height + radius / 2) return false;

			var forward = rotation * Vector3.forward;
			var rotationAngle = Mathf.Atan2(forward.z, forward.x) + (Mathf.PI * 0.5f);
			rotationAngle += transform.eulerAngles.y * Mathf.Deg2Rad;
			//on an angle, the rect needs to be larger
			radius *= Mathf.Sqrt(forward.z * forward.z + forward.x * forward.x);
			
			UpdateShaderValues(targetValue, opacity, rotationAngle);
			
			RenderTexture.active = _renderTexture;
			//_renderTexture.DiscardContents();
			GL.PushMatrix();
			GL.LoadPixelMatrix(0, _renderTexture.width, _renderTexture.height, 0);
			Graphics.DrawTexture(
				new Rect(texturePos - new Vector2(radius, radius), new Vector2(radius * 2, radius * 2)),
				brush, new Rect(0, 0, 1, 1), 0, 0, 0, 0, _blitMaterial
			);
			GL.PopMatrix();
			RenderTexture.active = null;

			return true;
		}

		public void Initialize()
		{
            Debug.Log($"initializing");

			//set up all the various things we need for drawing later
			_blitMaterial = new Material(Resources.Load<Shader>("PaintShader"));
			_renderTexture = HeightBlend.Instance.GetRenderTexture();
			_mainMaterial = GetComponent<Renderer>().material;
			_mainMaterial.SetTexture(MaskTex, _renderTexture);
			_initialized = true;
			Clear();
		}

		public void OnDestroy()
		{
			Destroy(_renderTexture);
			Destroy(_mainMaterial);
		}

		
		/// <summary>
		/// Clears the render texture to black or white
		/// </summary>
		/// <param name="black">Sets the texture to black if true, white if false</param>
		[ContextMenu("Clear")]
		public void Clear(bool black = true)
		{
			UpdateShaderValues(black ? 0f : 1f, 1f, 0f);
			
			RenderTexture.active = _renderTexture;
			//_renderTexture.DiscardContents();
			GL.PushMatrix();
			GL.LoadPixelMatrix(0, _renderTexture.width, _renderTexture.height, 0);
			Graphics.DrawTexture(
				new Rect(new Vector2(0f, 0f), new Vector2(_renderTexture.width, _renderTexture.height)),
				Texture2D.whiteTexture, new Rect(0, 0, 1, 1), 0, 0, 0, 0, _blitMaterial
			);
			GL.PopMatrix();
			RenderTexture.active = null;
		}
	
		private void UpdateShaderValues(float targetValue, float opacity, float angle)
		{
			_blitMaterial.SetFloat(TargetValue, targetValue);
			_blitMaterial.SetFloat(Opacity, opacity);
			_blitMaterial.SetFloat(Rotation, angle);
		}
		
		//turns local position to UV coordinates (0-1, 0-1)
		//note that this assumes the model is set up such that y = up.
		//this is important! A z=up plane will not work!
		private Vector2 GetUvPosition(Vector3 localPosition)
		{
			localPosition.x *= -1f;
			localPosition.x = localPosition.x + 0.5f;
			localPosition.z = localPosition.z + 0.5f;
	
			return new Vector2(localPosition.x, localPosition.z);
		}

		//transforms a UV coordinate into a texture position
		private Vector2Int GetTexturePosition(Vector2 uvPos)
		{
			return new Vector2Int(Mathf.FloorToInt(uvPos.x * _renderTexture.width), Mathf.FloorToInt(uvPos.y * _renderTexture.height));
		}
	}
}